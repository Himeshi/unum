/*
 * Compile with gcc vector_multiply.c -o vector_multiply -lunum -lgmp -lmpfr -lm
 */
#include <stdio.h>
#include <unum.h>

int main () {
	set_env(3,5);
	
	_g_fsizemax = 23;

	ubound_t a1, b1, product;
	
	ubound_init(&a1);
	ubound_init(&b1);
	ubound_init(&product);
	
	x2ub(3.2E+8, &a1);
	x2ub(4E+7, &b1);
	
	timesubound(&product, a1, b1);
	
	uboundview(product);
	return 0;
}