#include "unum.h"

void unum_mul(unum_t* result, unum_t* op1, unum_t* op2)
{

    unsigned short bias_of_op1, bias_of_op2, trailing_zeros, result_fraction_size;
    int exponent_of_op1, exponent_of_op2, exponent_of_result, fraction_bit_length, fraction_scale, smallnormalu_exp,
        op1_length, op2_length;
    unsigned long long multiplied_fractions = 0, op1_num, op2_num;
    mpz_t mpz_result, mpz_op2;

    // ubit
    (*result).ubit = (*op1).ubit | (*op2).ubit;

    // exponent and fraction
    // bias_of_op1 = pow(2, (*op1).e_size) - 1;
    // fraction
    // op1_num = (*op1).fraction + pow(2, ((*op1).f_size + 1));
    // op2_num = (*op2).fraction + pow(2, ((*op2).f_size + 1));
    bias_of_op1 = (1 << (*op1).e_size) - 1;
    if ((*op1).exponent) {
        exponent_of_op1 = (*op1).exponent - bias_of_op1;
        op1_num = (*op1).fraction | (1ULL << ((*op1).f_size + 1));
    } else {
        exponent_of_op1 = 1 - bias_of_op1;
        op1_num = (*op1).fraction;
    }

    // bias_of_op2 = pow(2, (*op2).e_size) - 1;
    bias_of_op2 = (1 << (*op2).e_size) - 1;
    if ((*op2).exponent) {
        exponent_of_op2 = (*op2).exponent - bias_of_op2;
        op2_num = (*op2).fraction | (1ULL << ((*op2).f_size + 1));
    } else {
        exponent_of_op2 = 1 - bias_of_op2;
        op2_num = (*op2).fraction;
    }

    exponent_of_result = exponent_of_op1 + exponent_of_op2;
    result_fraction_size = (*op1).f_size + (*op2).f_size + 2;
    op1_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(op1_num);
    op2_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(op2_num);

    if (op1_length + op2_length <= INTERMEDIATE_FRACTION_SIZE) {
        multiplied_fractions = (op1_num * op2_num);
        fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(multiplied_fractions);
    } else {
        mpz_init(mpz_result);
        mpz_init(mpz_op2);
        mpz_import(mpz_result, 1, -1, sizeof op1_num, 0, 0, &op1_num);
        mpz_import(mpz_op2, 1, -1, sizeof op2_num, 0, 0, &op2_num);
        mpz_mul(mpz_result, mpz_result, mpz_op2);
        fraction_bit_length = (int)mpz_sizeinbase(mpz_result, 2);
        mpz_clear(mpz_op2);
    }

    // normalize the fraction and get the exponent accordingly
    if (multiplied_fractions) {
        trailing_zeros = __builtin_ctzll(multiplied_fractions);
        if (trailing_zeros > 0 && !(*result).ubit) {
            multiplied_fractions >>= trailing_zeros;
            exponent_of_result += trailing_zeros;
            fraction_bit_length -= trailing_zeros;
            trailing_zeros = 0;
        }

        if (fraction_bit_length > (_g_fsizemax + 1)) {
            // first adjust the numerator of the fraction
            multiplied_fractions >>= (fraction_bit_length - _g_fsizemax - 1);
            exponent_of_result += (fraction_bit_length - _g_fsizemax - 1);
            fraction_bit_length = _g_fsizemax + 1;
            (*result).ubit = 1;
            // next adjust the denominator of the fraction
            exponent_of_result += (_g_fsizemax - result_fraction_size);
            result_fraction_size = _g_fsizemax;
        } else {
            // in this case we only need to adjust the denominator
            exponent_of_result += (fraction_bit_length - result_fraction_size - 1);
            result_fraction_size = fraction_bit_length - 1;
        }
    } else if (mpz_sgn(mpz_result)) {
        trailing_zeros = mpz_scan1(mpz_result, 0);
        if (trailing_zeros > 0 && !(*result).ubit) {
            mpz_tdiv_q_2exp(mpz_result, mpz_result, trailing_zeros);
            exponent_of_result += trailing_zeros;
            fraction_bit_length -= trailing_zeros;
            trailing_zeros = 0;
        }

        if (fraction_bit_length > (_g_fsizemax + 1)) {
            mpz_tdiv_q_2exp(mpz_result, mpz_result, (fraction_bit_length - _g_fsizemax - 1));
            exponent_of_result += (fraction_bit_length - _g_fsizemax - 1);
            fraction_bit_length = _g_fsizemax + 1;
            (*result).ubit = 1;
            exponent_of_result += (_g_fsizemax - result_fraction_size);
            result_fraction_size = _g_fsizemax;
        } else {
            exponent_of_result += (fraction_bit_length - result_fraction_size - 1);
            result_fraction_size = fraction_bit_length - 1;
        }
        mpz_export(&multiplied_fractions, 0, -1, sizeof multiplied_fractions, 0, 0, mpz_result);
        mpz_clear(mpz_result);
    } else {
        (*result) = _g_zero;
        return;
    }

    // result is greater than what can be expressed
    if (exponent_of_result > _g_maxexp ||
        (exponent_of_result == _g_maxexp && multiplied_fractions >= _g_maxfracvalhidden)) {
        (*result) = _g_maxrealu;
        // when the answer is inexact maxrealu, a bit from the fraction can be
        // thrown out
        if (_g_fsizesize > 0) {
            (*result).fraction >>= 1;
            (*result).f_size -= 1;
        }
        (*result).sign = op1->sign ^ op2->sign;
        (*result).ubit = true;
        return;
    }

    // result is smaller than what can be expressed
    // no need to check for fractions with _g_minexp since the minimum fraction
    // is that of _g_smallsubnormalu so there cannot be a number with _g_minexp
    // as exponent and a fraction less than the fraction of _g_smallsubnormalu
    if (exponent_of_result < _g_minexp) {
        (*result) = _g_smallsubnormalu;
        (*result).fraction--;
        (*result).sign = op1->sign ^ op2->sign;
        (*result).ubit = true;
        return;
    }

    // if the result is subnormal
    // if the exponent is less than exponent of smallnormalu, then the result is subnormal
    // smallnormalu_exp = 1 - (pow(2, (_g_esizemax - 1)) - 1);
    smallnormalu_exp = 1 - ((1 << (_g_esizemax - 1)) - 1);
    if (exponent_of_result < smallnormalu_exp) {
        (*result).exponent = 0;
        (*result).e_size = _g_esizemax - 1;
        (*result).sign = ((*op1).sign ^ (*op2).sign);

        // fix the exponent to 1-bias and calculate the bits of the fraction
        fraction_scale = smallnormalu_exp - exponent_of_result;
        fraction_bit_length = result_fraction_size + fraction_scale;

        // set the fraction
        if (fraction_bit_length > _g_fsizemax) {
            // if result is larger than fsizemax and/or is inexact
            (*result).ubit = 1;
            // shift until it fits _g_fsizemax bits
            (*result).fraction = multiplied_fractions >> (fraction_bit_length - _g_fsizemax);
            (*result).f_size = _g_fsizemax - 1;
        } else {
            (*result).fraction = multiplied_fractions;
            (*result).f_size = fraction_bit_length - 1;
        }
        return;
    }

    // handling normal numbers

    // sign
    (*result).sign = ((*op1).sign ^ (*op2).sign);

    // fraction
    // reset the leading bit of the multiplied fraction (hidden bit))
    multiplied_fractions &= ~(1ULL << (INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(multiplied_fractions) - 1));
    fraction_bit_length = result_fraction_size; // this length already accounts for the hidden 1

    // set the fraction
    if (multiplied_fractions == 0 && (*result).ubit == 0) {
        (*result).fraction = 0;
        (*result).f_size = 0;
    } else {
        (*result).fraction = multiplied_fractions;
        (*result).f_size = fraction_bit_length - 1;
    }

    // exponent
    if (!exponent_of_result && !(*result).fraction && !(*result).ubit) {
        (*result).fraction = 1;
        (*result).f_size = 0;
        (*result).e_size = 0;
        (*result).exponent = 0;
        return;
    } else if (exponent_of_result == 1) {
        (*result).e_size = 0;
        (*result).exponent = 1;
    } else {
        (*result).e_size = INTERMEDIATE_EXP_SIZE - __builtin_clz(abs(exponent_of_result - 1));
        //(*result).exponent = exponent_of_result + (pow(2, ((*result).e_size)) - 1);
        (*result).exponent = exponent_of_result + ((1 << (*result).e_size) - 1);
    }

    // in case the value is bordering infinity, but shouldn't
    // if (((*result).exponent != _g_maxexpval || (*result).fraction != (_g_maxfracval - 1)) && (*result).exponent ==
    // (pow(2, ((*result).e_size + 1)) - 1) && (*result).fraction == (pow(2, ((*result).f_size + 1)) - 1) &&
    // (*result).ubit) {
    if (((*result).exponent != _g_maxexpval || (*result).fraction != (_g_maxfracval - 1)) &&
        (*result).exponent == ((1 << ((*result).e_size + 1)) - 1) &&
        (*result).fraction == ((1ULL << ((*result).f_size + 1)) - 1) && (*result).ubit) {
        // if fraction can be increased, increase that
        if ((*result).f_size < (_g_fsizemax - 1)) {
            (*result).f_size++;
            (*result).fraction = (*result).fraction << 1;
        } else {
            (*result).e_size++;
            //(*result).exponent = exponent_of_result + (pow(2, ((*result).e_size)) - 1);
            (*result).exponent = exponent_of_result + ((1 << (*result).e_size) - 1);
        }
    }

    // when the answer is inexact maxrealu, remove the bit from the fraction
    if (isPosOrNegInexactMaxreal((*result)) && _g_fsizesize > 0) {
        (*result).fraction >>= 1;
        (*result).f_size -= 1;
    }

    return;
}

void timesposleft(gbound_t* result, unum_t x, bool xb, unum_t y, bool yb)
{
    if (isPosOrNegZero(x) && xb == 0) {
        if (isInf(y) && yb == 0) {
            (*result).left_bound = _g_qNaNu;
            (*result).left_open = 1;
            return;
        } else {
            (*result).left_bound = _g_zero;
            (*result).left_open = 0;
            return;
        }
    }

    if (isPosOrNegZero(y) && yb == 0) {
        if (isInf(x) && xb == 0) {
            (*result).left_bound = _g_qNaNu;
            (*result).left_open = 1;
            return;
        } else {
            (*result).left_bound = _g_zero;
            (*result).left_open = 0;
            return;
        }
    }

    if (isPosOrNegZero(x) && xb == 1) {
        if (isInf(y) && yb == 0) {
            (*result).left_bound = _g_posinfu;
            (*result).left_open = 0;
            return;
        } else {
            (*result).left_bound = _g_zero;
            (*result).left_open = 1;
            return;
        }
    }

    if (isPosOrNegZero(y) && yb == 1) {
        if (isInf(x) && xb == 0) {
            (*result).left_bound = _g_posinfu;
            (*result).left_open = 0;
            return;
        } else {
            (*result).left_bound = _g_zero;
            (*result).left_open = 1;
            return;
        }
    }

    if ((isInf(x) && xb == 0) || (isInf(y) && yb == 0)) {
        (*result).left_bound = _g_posinfu;
        (*result).left_open = 0;
        return;
    }

    unum_mul(&(result->left_bound), &x, &y);
    (*result).left_open = xb | yb;

    //    if (!unum_compare(result->left_bound, _g_qNaNu) && !unum_compare(result->left_bound, _g_sNaNu)) {
    //        if (result->left_bound.ubit == 1) {
    //            result->left_bound.ubit = 0;
    //            result->left_open = 1;
    //        }
    //    }
    return;
}

void timesposright(gbound_t* result, unum_t x, bool xb, unum_t y, bool yb)
{
    if (isInf(x) && xb == 0) {
        if (isPosOrNegZero(y) && yb == 0) {
            (*result).right_bound = _g_qNaNu;
            (*result).right_open = 1;
            return;
        } else {
            (*result).right_bound = _g_posinfu;
            (*result).right_open = 0;
            return;
        }
    }

    if (isInf(y) && yb == 0) {
        if (isPosOrNegZero(x) && xb == 0) {
            (*result).right_bound = _g_qNaNu;
            (*result).right_open = 1;
            return;
        } else {
            (*result).right_bound = _g_posinfu;
            (*result).right_open = 0;
            return;
        }
    }

    if (isInf(x) && xb == 1) {
        if (isPosOrNegZero(y) && yb == 0) {
            (*result).right_bound = _g_zero;
            (*result).right_open = 0;
            return;
        } else {
            (*result).right_bound = _g_posinfu;
            (*result).right_open = 1;
            return;
        }
    }

    if (isInf(y) && yb == 1) {
        if (isPosOrNegZero(x) && xb == 0) {
            (*result).right_bound = _g_zero;
            (*result).right_open = 0;
            return;
        } else {
            (*result).right_bound = _g_posinfu;
            (*result).right_open = 1;
            return;
        }
    }

    if ((isPosOrNegZero(x) && xb == 0) || (isPosOrNegZero(y) && yb == 0)) {
        (*result).right_bound = _g_zero;
        (*result).right_open = 0;
        return;
    }

    unum_mul(&(result->right_bound), &x, &y);
    (*result).right_open = xb | yb;

    //    if (!unum_compare(result->right_bound, _g_qNaNu) && !unum_compare(result->right_bound, _g_sNaNu)) {
    //        if (result->right_bound.ubit == 1) {
    //            result->right_bound.ubit = 0;
    //            result->right_open = 1;
    //        }
    //    }
    return;
}

void timesg(gbound_t* result, gbound_t op1, gbound_t op2)
{

    unum_temp_t lcan[4], rcan[4], temp;
    unum_t op1_temp, op2_temp;
    gbound_t result_temp;
    int lcan_size = 0, rcan_size = 0;
    int i;

    if (isNaN(op1.left_bound) || isNaN(op1.right_bound) || isNaN(op2.left_bound) || isNaN(op2.right_bound)) {
        (*result).left_bound = _g_qNaNu;
        (*result).left_open = 1;
        (*result).right_bound = _g_qNaNu;
        (*result).right_open = 1;
        return;
    }

    // Lower left corner in upper right quadrant, facing uphill
    if (isGreaterThanOrEqualToZero(op1.left_bound) && isGreaterThanOrEqualToZero(op2.left_bound)) {
        timesposleft(&result_temp, op1.left_bound, op1.left_open, op2.left_bound, op2.left_open);
        temp.u = result_temp.left_bound;
        temp.is_open = result_temp.left_open;
        lcan[lcan_size] = temp;
        lcan_size++;
    }

    // Upper right corner in lower left quadrant, facing uphill
    if ((isLessThanZero(op1.right_bound) || (isPosOrNegZero(op1.right_bound) && op1.right_open)) &&
        (isLessThanZero(op2.right_bound) || (isPosOrNegZero(op2.right_bound) && op2.right_open))) {
        op1_temp = op1.right_bound;
        if (!isPosOrNegZero(op1_temp) || op1.right_open)
            op1_temp.sign = !op1_temp.sign;
        op2_temp = op2.right_bound;
        if (!isPosOrNegZero(op2_temp) || op2.right_open)
            op2_temp.sign = !op2_temp.sign;
        timesposleft(&result_temp, op1_temp, op1.right_open, op2_temp, op2.right_open);
        temp.u = result_temp.left_bound;
        temp.is_open = result_temp.left_open;

        lcan[lcan_size] = temp;
        lcan_size++;
    }

    // Upper left corner in upper left quadrant, facing uphill
    if ((isLessThanZero(op1.left_bound) || (isPosOrNegZero(op1.left_bound) && !op1.left_open)) &&
        (isGreaterThanZero(op2.right_bound) || (isPosOrNegZero(op2.right_bound) && !op2.right_open))) {
        op1_temp = op1.left_bound;
        if (!isPosOrNegZero(op1_temp) || op1.left_open)
            op1_temp.sign = !op1_temp.sign;
        timesposright(&result_temp, op1_temp, op1.left_open, op2.right_bound, op2.right_open);
        temp.u = result_temp.right_bound;
        temp.is_open = result_temp.right_open;
        // negate
        if (!isPosOrNegZero(temp.u) || temp.is_open)
            temp.u.sign = !temp.u.sign;

        lcan[lcan_size] = temp;
        lcan_size++;
    }

    // Lower right corner in lower right quadrant, facing uphill
    if ((isGreaterThanZero(op1.right_bound) || (isPosOrNegZero(op1.right_bound) && !op1.right_open)) &&
        (isLessThanZero(op2.left_bound) || (isPosOrNegZero(op2.left_bound) && !op2.left_open))) {
        op2_temp = op2.left_bound;
        if (!isPosOrNegZero(op2_temp) || op2.left_open)
            op2_temp.sign = !op2_temp.sign;
        timesposright(&result_temp, op1.right_bound, op1.right_open, op2_temp, op2.left_open);
        temp.u = result_temp.right_bound;
        temp.is_open = result_temp.right_open;
        if (!isPosOrNegZero(temp.u) || temp.is_open) {
            temp.u.sign = !temp.u.sign;
        }
        lcan[lcan_size] = temp;
        lcan_size++;
    }

    // Upper right corner in upper right quadrant, facing downhill
    if ((isGreaterThanZero(op1.right_bound) || (isPosOrNegZero(op1.right_bound) && !op1.right_open)) &&
        (isGreaterThanZero(op2.right_bound) || (isPosOrNegZero(op2.right_bound) && !op2.right_open))) {
        timesposright(&result_temp, op1.right_bound, op1.right_open, op2.right_bound, op2.right_open);
        temp.u = result_temp.right_bound;
        temp.is_open = result_temp.right_open;
        rcan[rcan_size] = temp;
        rcan_size++;
    }

    // Lower left corner in lower left quadrant, facing downhill
    if ((isLessThanZero(op1.left_bound) || (isPosOrNegZero(op1.left_bound) && !op1.left_open)) &&
        (isLessThanZero(op2.left_bound) || (isPosOrNegZero(op2.left_bound) && !op2.left_open))) {
        op1_temp = op1.left_bound;
        if (!isPosOrNegZero(op1_temp) || op1.left_open)
            op1_temp.sign = !op1_temp.sign;
        op2_temp = op2.left_bound;
        if (!isPosOrNegZero(op2_temp) || op2.left_open)
            op2_temp.sign = !op2_temp.sign;
        timesposright(&result_temp, op1_temp, op1.left_open, op2_temp, op2.left_open);
        temp.u = result_temp.right_bound;
        temp.is_open = result_temp.right_open;
        rcan[rcan_size] = temp;
        rcan_size++;
    }

    // Lower right corner in upper left quadrant, facing downhill
    if ((isLessThanZero(op1.right_bound) || (isPosOrNegZero(op1.right_bound) && op1.right_open)) &&
        isGreaterThanOrEqualToZero(op2.left_bound)) {
        op1_temp = op1.right_bound;
        if (!isPosOrNegZero(op1_temp) || op1.right_open)
            op1_temp.sign = !op1_temp.sign;
        timesposleft(&result_temp, op1_temp, op1.right_open, op2.left_bound, op2.left_open);
        temp.u = result_temp.left_bound;
        temp.is_open = result_temp.left_open;
        if (!isPosOrNegZero(temp.u) || temp.is_open)
            temp.u.sign = !temp.u.sign;
        rcan[rcan_size] = temp;
        rcan_size++;
    }

    // Upper left corner in lower right quadrant, facing downhill
    if (isGreaterThanOrEqualToZero(op1.left_bound) &&
        (isLessThanZero(op2.right_bound) || (isPosOrNegZero(op2.right_bound) && op2.right_open))) {
        op2_temp = op2.right_bound;
        if (!isPosOrNegZero(op2_temp) || op2.right_open)
            op2_temp.sign = !op2_temp.sign;
        timesposleft(&result_temp, op1.left_bound, op1.left_open, op2_temp, op2.right_open);
        temp.u = result_temp.left_bound;
        temp.is_open = result_temp.left_open;
        if (!isPosOrNegZero(temp.u) || temp.is_open)
            temp.u.sign = !temp.u.sign;
        rcan[rcan_size] = temp;
        rcan_size++;
    }

    for (i = 0; i < lcan_size; i++) {
        if (isNaN(lcan[i].u)) {
            (*result).left_bound = _g_qNaNu;
            (*result).left_open = 1;
            (*result).right_bound = _g_qNaNu;
            (*result).right_open = 1;
            return;
        }
    }

    for (i = 0; i < rcan_size; i++) {
        if (isNaN(rcan[i].u)) {
            (*result).left_bound = _g_qNaNu;
            (*result).left_open = 1;
            (*result).right_bound = _g_qNaNu;
            (*result).right_open = 1;
            return;
        }
    }

    if (lcan_size == 1 && rcan_size == 1) {
        (*result).left_bound = lcan[0].u;
        (*result).right_bound = rcan[0].u;
        (*result).left_open = lcan[0].is_open;
        (*result).right_open = rcan[0].is_open;
    } else {
        // find the minimum value (and the second lowest) from lcan
        int min_index = 0, second_min_index = 0, max_index = 0, second_max_index = 0;

        float min_value = u2f(lcan[0].u);
        for (i = 1; i < lcan_size; i++) {
            if (u2f(lcan[i].u) < min_value) {
                min_index = i;
                min_value = u2f(lcan[i].u);
            }

            if (min_value < u2f(lcan[i].u) && u2f(lcan[i].u) < u2f(lcan[second_min_index].u)) {
                second_min_index = i;
            }
        }

        // find maximum value (and the second highest) from rcan
        float max_value = u2f(rcan[0].u);
        for (i = 1; i < rcan_size; i++) {
            if (u2f(rcan[i].u) > max_value) {
                max_index = i;
                max_value = u2f(rcan[i].u);
            }

            if (max_value > u2f(rcan[i].u) && u2f(rcan[i].u) < u2f(rcan[second_max_index].u)) {
                second_max_index = i;
            }
        }

        // assign values to result
        (*result).left_bound = lcan[min_index].u;
        (*result).left_open = lcan[min_index].is_open;
        (*result).right_bound = rcan[max_index].u;
        (*result).right_open = rcan[max_index].is_open;

        // if the magnitude of second highest is equal to the highest and one of
        // values is exact, mark the right endpoint as closed
        if (unum_compare(lcan[min_index].u, lcan[second_min_index].u) &&
            (lcan[min_index].is_open == 0 || lcan[second_min_index].is_open == 0)) {
            (*result).left_open = 0;
        }

        // if the magnitude of second lowest is equal to the lowest and one of
        // values is exact, mark the left endpoint as closed
        if (unum_compare(rcan[max_index].u, rcan[second_max_index].u) &&
            (rcan[max_index].is_open == 0 || rcan[second_max_index].is_open == 0)) {
            (*result).right_open = 0;
        }
    }
    return;
}

void timesu(ubound_t* result, unum_t op1, unum_t op2)
{
#ifdef BIT_TRACK
    int op1_bits, op2_bits;
    op1_bits = 1 + op1.e_size + 1 + op1.f_size + 1 + _g_utagsize;
    op2_bits = 1 + op2.e_size + 1 + op2.f_size + 1 + _g_utagsize;
    total_bit_count += (op1_bits + op2_bits);
    total_op_count += 2;
    if (op1_bits > max_bit_count)
        max_bit_count = op1_bits;
    if (op2_bits > max_bit_count)
        max_bit_count = op2_bits;
    if (op1_bits < min_bit_count)
        min_bit_count = op1_bits;
    if (op2_bits < min_bit_count)
        min_bit_count = op2_bits;
#endif

    gbound_t gbound_op1, gbound_op2, gbound_result;
    unum_t temp;

    // if both numbers are exact, multiply and return
    if (op1.ubit == 0 && op2.ubit == 0) {
        // if both numbers are not positive or negative infinity or zero, simply multiply
        if (!isNaNOrInf(op1) && !isNaNOrInf(op2) && !isPosOrNegZero(op1) && !isPosOrNegZero(op2)) {
            unum_mul(&temp, &op1, &op2);

#ifdef BIT_TRACK
            int bits = 1 + temp.e_size + 1 + temp.f_size + 1 + _g_utagsize;
            total_bit_count += bits;
            total_op_count += 1;
            if (bits > max_bit_count)
                max_bit_count = bits;
            if (bits < min_bit_count)
                min_bit_count = bits;
#endif
            result->left_bound = temp;
            result->right_bound = temp;
            return;
        }

        // if both ops are some kind of infinity
        if (isNaNOrInf(op1) && isNaNOrInf(op2)) {
            result->left_bound = _g_posinfu;
            result->left_bound.sign = op1.sign ^ op2.sign;
            result->right_bound = result->left_bound;

#ifdef BIT_TRACK
            total_bit_count += _g_maxubits;
            total_op_count += 1;
            if (_g_maxubits > max_bit_count)
                max_bit_count = _g_maxubits;
#endif
            return;
        }

        if (isPosOrNegZero(op1) || isPosOrNegZero(op2)) {
            if (isNaNOrInf(op1) || isNaNOrInf(op2)) {
                result->left_bound = _g_qNaNu;
                result->right_bound = _g_qNaNu;
                return;
            } else {
                result->left_bound = _g_zero;
                result->right_bound = _g_zero;
#ifdef BIT_TRACK
                int bits = 3 + _g_utagsize;
                total_bit_count += bits;
                total_op_count += 1;
                if (bits > max_bit_count)
                    max_bit_count = bits;
                if (bits < min_bit_count)
                    min_bit_count = bits;
#endif
                return;
            }
        }
    }

    // if either of the unums are inexact, then extract the bounds and divide
    get_gbound_from_unum(&gbound_op1, &op1);
    get_gbound_from_unum(&gbound_op2, &op2);

    if (isNaN(gbound_op1.left_bound) || isNaN(gbound_op2.left_bound) || isNaN(gbound_op1.right_bound) ||
        isNaN(gbound_op2.right_bound)) {
        result->left_bound = _g_qNaNu;
        result->right_bound = _g_qNaNu;
        return;
    }

    // multiply
    timesg(&gbound_result, gbound_op1, gbound_op2);

    // if the result contains NaNs
    if (isNaN(gbound_result.left_bound) || isNaN(gbound_result.right_bound)) {
        result->left_bound = _g_qNaNu;
        result->right_bound = _g_qNaNu;
        return;
    }

    get_ubound_result_from_gbound(result, &gbound_result);

#ifdef BIT_TRACK
    int bits;
    bits = 3 + result->left_bound.e_size + result->left_bound.f_size + _g_utagsize;
    if (!unum_compare(result->left_bound, result->right_bound))
        bits += (3 + result->right_bound.e_size + result->right_bound.f_size + _g_utagsize);
    total_bit_count += bits;
    total_op_count += 1;
    if (bits > max_bit_count)
        max_bit_count = bits;
    if (bits < min_bit_count)
        min_bit_count = bits;
#endif

    return;
}

void timesubound(ubound_t* result, ubound_t op1, ubound_t op2)
{
    gbound_t gbound_op1, gbound_op2, gbound_result;

#ifdef BIT_TRACK
    int bits;
    bits = 3 + op1.left_bound.e_size + op1.left_bound.f_size + _g_utagsize;
    if (!unum_compare(op1.left_bound, op1.right_bound))
        bits += (3 + op1.right_bound.e_size + op1.right_bound.f_size + _g_utagsize);
    total_bit_count += bits;
    total_op_count += 1;
    if (bits > max_bit_count)
        max_bit_count = bits;
    if (bits < min_bit_count)
        min_bit_count = bits;

    bits = 3 + op2.left_bound.e_size + op2.left_bound.f_size + _g_utagsize;
    if (!unum_compare(op2.left_bound, op2.right_bound))
        bits += (3 + op2.right_bound.e_size + op2.right_bound.f_size + _g_utagsize);
    total_bit_count += bits;
    total_op_count += 1;
    if (bits > max_bit_count)
        max_bit_count = bits;
    if (bits < min_bit_count)
        min_bit_count = bits;
#endif

    get_gbound_from_ubound(&gbound_op1, &op1);
    get_gbound_from_ubound(&gbound_op2, &op2);

    if (isNaN(gbound_op1.left_bound) || isNaN(gbound_op2.left_bound) || isNaN(gbound_op1.right_bound) ||
        isNaN(gbound_op2.right_bound)) {
        result->left_bound = _g_qNaNu;
        result->right_bound = _g_qNaNu;
        return;
    }

    if (unum_compare(gbound_op1.left_bound, gbound_op1.right_bound) &&
        unum_compare(gbound_op2.left_bound, gbound_op2.right_bound)) {
        timesu(result, gbound_op1.left_bound, gbound_op2.left_bound);
#ifdef BIT_TRACK
        bits = 3 + op1.left_bound.e_size + op1.left_bound.f_size + _g_utagsize;
        total_bit_count -= bits;
        total_op_count -= 1;
        bits = 3 + op2.left_bound.e_size + op2.left_bound.f_size + _g_utagsize;
        total_bit_count -= bits;
        total_op_count -= 1;
#endif
        return;
    }

    // multiply
    timesg(&gbound_result, gbound_op1, gbound_op2);

    // if the result contains NaNs
    if (isNaN(gbound_result.left_bound) || isNaN(gbound_result.right_bound)) {
        result->left_bound = _g_qNaNu;
        result->right_bound = _g_qNaNu;
        return;
    }

    get_ubound_result_from_gbound(result, &gbound_result);

#ifdef BIT_TRACK
    bits = 3 + result->left_bound.e_size + result->left_bound.f_size + _g_utagsize;
    if (!unum_compare(result->left_bound, result->right_bound))
        bits += (3 + result->right_bound.e_size + result->right_bound.f_size + _g_utagsize);
    total_bit_count += bits;
    total_op_count += 1;
    if (bits > max_bit_count)
        max_bit_count = bits;
    if (bits < min_bit_count)
        min_bit_count = bits;
#endif

    return;
}
