#include "unum.h"

void unum_sqrt(unum_t* result, unum_t* u)
{
    unsigned short bias_of_op, result_fraction_size, trailing_zeros;
    int exponent_of_result, smallnormalu_exp, fraction_scale;
    unsigned long long result_fraction, temp_fraction;
    unsigned int numerator_scale, fraction_bit_length;
    mpz_t mpz_result, mpz_rem;

    // square root of zero
    if (isPosOrNegZero((*u))) {
        (*result) = _g_zero;
        return;
    }

    // square root of a negative number
    if ((*u).sign) {
        (*result) = _g_qNaNu;
        return;
    }

    // square root of infinity
    if (isInf((*u))) {
        (*result) = _g_posinfu;
        return;
    }

    (*result).sign = 0;

    bias_of_op = (1 << (*u).e_size) - 1;
    if ((*u).exponent) {
        exponent_of_result = (*u).exponent - bias_of_op;
        result_fraction = (*u).fraction | (1 << ((*u).f_size + 1));
    } else {
        exponent_of_result = 1 - bias_of_op;
        result_fraction = (*u).fraction;
    }
    result_fraction_size = (*u).f_size + 1;

    numerator_scale = (_g_fsizemax + 1) << 1;
    if (numerator_scale <= INTERMEDIATE_FRACTION_SIZE) {
        numerator_scale -= (INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(result_fraction));
        temp_fraction = result_fraction << numerator_scale;
        exponent_of_result -= numerator_scale;

        if (IS_ODD(result_fraction_size) && !IS_ODD(exponent_of_result)) {
            temp_fraction = temp_fraction >> 1;
            // exponent_of_result++ (now both are odd)
            result_fraction_size++;
            exponent_of_result += 2;
        } else if (!IS_ODD(result_fraction_size) && IS_ODD(exponent_of_result)) {
            temp_fraction = temp_fraction >> 1;
            exponent_of_result++;
        }

        result_fraction = sqrt32(temp_fraction);
        if (result_fraction * result_fraction == temp_fraction)
            (*result).ubit = 0;
        else
            (*result).ubit = 1;
    } else {
        mpz_init(mpz_result);
        mpz_init(mpz_rem);
        mpz_import(mpz_result, 1, -1, sizeof result_fraction, 0, 0, &result_fraction);
        numerator_scale -= (INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(result_fraction));
        mpz_mul_2exp(mpz_result, mpz_result, numerator_scale);
        exponent_of_result -= numerator_scale;

        if (IS_ODD(result_fraction_size) && !IS_ODD(exponent_of_result)) {
            mpz_tdiv_q_2exp(mpz_result, mpz_result, 1);
            // exponent_of_result++ (now both are odd)
            result_fraction_size++;
            exponent_of_result += 2;
        } else if (!IS_ODD(result_fraction_size) && IS_ODD(exponent_of_result)) {
            mpz_tdiv_q_2exp(mpz_result, mpz_result, 1);
            exponent_of_result++;
        }

        mpz_sqrtrem(mpz_result, mpz_rem, mpz_result);
        mpz_export(&result_fraction, 0, -1, sizeof result_fraction, 0, 0, mpz_result);
        if (!mpz_sgn(mpz_rem))
            (*result).ubit = 0;
        else
            (*result).ubit = 1;
        mpz_clear(mpz_result);
        mpz_clear(mpz_rem);
    }

    if (IS_ODD(result_fraction_size) && IS_ODD(exponent_of_result)) {
        result_fraction_size++;
        exponent_of_result++;
    }
    result_fraction_size >>= 1;
    exponent_of_result >>= 1;
    fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(result_fraction);

    // normalize the fraction and get the exponent accordingly
    if (result_fraction) {
        trailing_zeros = __builtin_ctzll(result_fraction);
        if (trailing_zeros > 0 && !(result)->ubit) {
            result_fraction >>= trailing_zeros;
            exponent_of_result += trailing_zeros;
            fraction_bit_length -= trailing_zeros;
            trailing_zeros = 0;
        }

        if (fraction_bit_length > (_g_fsizemax + 1)) {
            // first adjust the numerator of the fraction
            result_fraction >>= (fraction_bit_length - _g_fsizemax - 1);
            exponent_of_result += (fraction_bit_length - _g_fsizemax - 1);
            fraction_bit_length = _g_fsizemax + 1;
            (*result).ubit = 1;
            // next adjust the denominator of the fraction
            exponent_of_result += (_g_fsizemax - result_fraction_size);
            result_fraction_size = _g_fsizemax;
        } else {
            // in this case we only need to adjust the denominator
            exponent_of_result += (fraction_bit_length - result_fraction_size - 1);
            result_fraction_size = fraction_bit_length - 1;
        }
    } else {
        (*result) = _g_zero;
        return;
    }

    // result is greater than what can be expressed
    if (exponent_of_result > _g_maxexp || (exponent_of_result == _g_maxexp && result_fraction >= _g_maxfracvalhidden)) {
        (*result) = _g_maxrealu;
        // when the answer is inexact maxrealu, a bit from the fraction can be
        // thrown out
        if (_g_fsizesize > 0) {
            (*result).fraction >>= 1;
            (*result).f_size -= 1;
        }
        (*result).ubit = true;
        return;
    }

    // result is smaller than what can be expressed
    // no need to check for fractions with _g_minexp since the minimum fraction
    // is that of _g_smallsubnormalu so there cannot be a number with _g_minexp
    // as exponent and a fraction less than the fraction of _g_smallsubnormalu
    if (exponent_of_result < _g_minexp) {
        (*result) = _g_smallsubnormalu;
        (*result).fraction--;
        (*result).ubit = true;
        return;
    }

    // if the result is subnormal
    // if the exponent is less than exponent of smallnormalu, then the result is subnormal
    // smallnormalu_exp = 1 - (pow(2, (_g_esizemax - 1)) - 1);
    smallnormalu_exp = 1 - ((1 << (_g_esizemax - 1)) - 1);
    if (exponent_of_result < smallnormalu_exp) {
        (*result).exponent = 0;
        (*result).e_size = _g_esizemax - 1;
        (*result).sign = 0;

        // fix the exponent to 1-bias and calculate the bits of the fraction
        fraction_scale = smallnormalu_exp - exponent_of_result;
        fraction_bit_length = result_fraction_size + fraction_scale;

        // set the fraction
        if (fraction_bit_length > _g_fsizemax) {
            // if result is larger than fsizemax and/or is inexact
            (*result).ubit = 1;
            // shift until it fits _g_fsizemax bits
            (*result).fraction = result_fraction >> (fraction_bit_length - _g_fsizemax);
            (*result).f_size = _g_fsizemax - 1;
        } else {
            (*result).fraction = result_fraction;
            (*result).f_size = fraction_bit_length - 1;
        }
        return;
    }

    // handling normal numbers

    (*result).sign = 0;

    // reset the leading bit of the multiplied fraction (hidden bit))
    result_fraction &= ~(1ULL << (INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(result_fraction) - 1));
    fraction_bit_length = result_fraction_size; // this length already accounts for the hidden 1

    // set the fraction
    if (result_fraction == 0 && (*result).ubit == 0) {
        (*result).fraction = 0;
        (*result).f_size = 0;
    } else {
        (*result).fraction = result_fraction;
        (*result).f_size = fraction_bit_length - 1;
    }

    // exponent
    if (!exponent_of_result && !(*result).fraction && !(*result).ubit) {
        (*result).fraction = 1;
        (*result).f_size = 0;
        (*result).e_size = 0;
        (*result).exponent = 0;
        return;
    } else if (exponent_of_result == 1) {
        (*result).e_size = 0;
        (*result).exponent = 1;
    } else {
        (*result).e_size = INTERMEDIATE_EXP_SIZE - __builtin_clz(abs(exponent_of_result - 1));
        //(*result).exponent = exponent_of_result + (pow(2, ((*result).e_size)) - 1);
        (*result).exponent = exponent_of_result + ((1 << (*result).e_size) - 1);
    }

    // in case the value is bordering infinity, but shouldn't
    // if (((*result).exponent != _g_maxexpval || (*result).fraction != (_g_maxfracval - 1)) && (*result).exponent ==
    // (pow(2, ((*result).e_size + 1)) - 1) && (*result).fraction == (pow(2, ((*result).f_size + 1)) - 1) &&
    // (*result).ubit) {
    if (((*result).exponent != _g_maxexpval || (*result).fraction != (_g_maxfracval - 1)) &&
        (*result).exponent == ((1 << ((*result).e_size + 1)) - 1) &&
        (*result).fraction == ((1 << ((*result).f_size + 1)) - 1) && (*result).ubit) {
        // if fraction can be increased, increase that
        if ((*result).f_size < (_g_fsizemax - 1)) {
            (*result).f_size++;
            (*result).fraction = (*result).fraction << 1;
        } else {
            (*result).e_size++;
            //(*result).exponent = exponent_of_result + (pow(2, ((*result).e_size)) - 1);
            (*result).exponent = exponent_of_result + ((1 << (*result).e_size) - 1);
        }
    }

    // when the answer is inexact maxrealu, remove the bit from the fraction
    if (isPosOrNegInexactMaxreal((*result)) && _g_fsizesize > 0) {
        (*result).fraction >>= 1;
        (*result).f_size -= 1;
    }

    return;
}

/*
 * Function obtained from http://www.codecodex.com/wiki/Calculate_an_integer_square_root#C
 */
unsigned long long sqrt32(unsigned long long n)
{
    unsigned long long c = 0x80000000;
    unsigned long long g = 0x80000000;

    for (;;) {
        if (g * g > n)
            g ^= c;
        c >>= 1;
        if (c == 0)
            return g;
        g |= c;
    }
}

void sqrtu(ubound_t* result, unum_t* op)
{

    gbound_t g_op, g_result;

    if (isNaN(*op) || ((*op).sign && !isPosOrNegZero(*op))) {
        (*result).left_bound = _g_qNaNu;
        (*result).right_bound = _g_qNaNu;
        return;
    }

    if (!(*op).ubit) {
        unum_sqrt(&(*result).left_bound, op);
        (*result).right_bound = (*result).left_bound;
        return;
    }

    get_gbound_from_unum(&g_op, op);

    unum_sqrt(&(g_result.left_bound), &(g_op.left_bound));
    unum_sqrt(&(g_result.right_bound), &(g_op.right_bound));
    g_result.left_open = g_op.left_open;
    g_result.right_open = g_op.right_open;

    get_ubound_result_from_gbound(result, &g_result);

    return;
}

void sqrtubound(ubound_t* result, ubound_t* op)
{

    gbound_t g_op, g_result;

    if (isNaN((*op).left_bound) || isNaN((*op).right_bound) ||
        ((*op).left_bound.sign && !isPosOrNegZero((*op).left_bound)) ||
        ((*op).right_bound.sign && !isPosOrNegZero((*op).right_bound))) {
        (*result).left_bound = _g_qNaNu;
        (*result).right_bound = _g_qNaNu;
        return;
    }

    if (unum_compare((*op).left_bound, (*op).right_bound)) {
        sqrtu(result, &(*op).left_bound);
        return;
    }

    if (!(*op).left_bound.ubit && !(*op).right_bound.ubit) {
        unum_sqrt(&(*result).left_bound, &(*op).left_bound);
        unum_sqrt(&(*result).right_bound, &(*op).right_bound);
        return;
    }

    get_gbound_from_ubound(&g_op, op);

    unum_sqrt(&(g_result.left_bound), &(g_op.left_bound));
    unum_sqrt(&(g_result.right_bound), &(g_op.right_bound));
    g_result.left_open = g_op.left_open;
    g_result.right_open = g_op.right_open;

    get_ubound_result_from_gbound(result, &g_result);

    return;
}
