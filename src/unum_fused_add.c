#include "unum.h"
#include "unum_fused.h"

void fused_unum_add(ubound_t* result, int size, unum_t list_a[]) {
	int i;
	fused_inter_t left_sum, right_sum;
	fused_inter_pair_t addition_list[size];
	gbound_t gbound_a, gbound_result;
	//smallsubnormalu exponent
	int bias = (1 << (_g_esizemax - 1)) - 2;

	//initialize
	mpz_init(left_sum.fraction);
	mpz_init(right_sum.fraction);
	left_sum.isInf = 0;
	right_sum.isInf = 0;
	left_sum.ubit = 0;
	right_sum.ubit = 0;
	left_sum.sign = 0;
	right_sum.sign = 0;

	//if everything is exact, then the list can be added directly
	//for now, since we're adding inexact numbers, this handling is left for later

	for (i = 0; i < size; i++) {
		if (!list_a[i].ubit) {
			get_fused_inter_t_from_unum(&addition_list[i].left, list_a[i]);

			//copy everything to the right
			addition_list[i].right.sign = addition_list[i].left.sign;
			addition_list[i].right.ubit = addition_list[i].left.ubit;
			addition_list[i].right.isInf = addition_list[i].left.isInf;
			addition_list[i].right.f_size = addition_list[i].left.f_size;
			addition_list[i].right.exponent = addition_list[i].left.exponent;
			mpz_init(addition_list[i].right.fraction);
			mpz_set(addition_list[i].right.fraction,
					addition_list[i].left.fraction);
		} else {
			get_gbound_from_unum(&gbound_a, &list_a[i]);
			get_fused_inter_t_from_unum(&addition_list[i].left,
					gbound_a.left_bound);
			addition_list[i].left.ubit = gbound_a.left_open;
			get_fused_inter_t_from_unum(&addition_list[i].right,
					gbound_a.right_bound);
			addition_list[i].right.ubit = gbound_a.right_open;
		}

		//make the fraction sizes the same
		mpz_mul_2exp(addition_list[i].left.fraction,
				addition_list[i].left.fraction,
				(_g_fsizemax - addition_list[i].left.f_size));
		mpz_mul_2exp(addition_list[i].right.fraction,
				addition_list[i].right.fraction,
				(_g_fsizemax - addition_list[i].right.f_size));

		//make the exponents the same
		mpz_mul_2exp(addition_list[i].left.fraction,
				addition_list[i].left.fraction,
				(addition_list[i].left.exponent + bias));
		mpz_mul_2exp(addition_list[i].right.fraction,
				addition_list[i].right.fraction,
				(addition_list[i].right.exponent + bias));

		//add & release memory of mpz_t
		//left endpoint
		if (!addition_list[i].left.isInf && !left_sum.isInf) {
			if (addition_list[i].left.sign) {
				mpz_neg(addition_list[i].left.fraction,
						addition_list[i].left.fraction);
			}
			mpz_add(left_sum.fraction, left_sum.fraction,
					addition_list[i].left.fraction);
			left_sum.ubit = left_sum.ubit | addition_list[i].left.ubit;
			if (mpz_sgn(left_sum.fraction) < 0) {
				left_sum.sign = 1;
			} else {
				left_sum.sign = 0;
			}
		} else if ((addition_list[i].left.isInf && !addition_list[i].left.ubit
				&& addition_list[i].left.sign)
				|| (left_sum.isInf && !left_sum.ubit && left_sum.sign)) {
			if ((addition_list[i].left.isInf && !addition_list[i].left.ubit
					&& !addition_list[i].left.sign)
					|| (left_sum.isInf && !left_sum.ubit && !left_sum.sign)) {
				printf("Result is NaN in left endpoint sum\n");
			} else {
				mpz_set_ui(left_sum.fraction, 0);
				left_sum.sign = 1;
				left_sum.ubit = 0;
				left_sum.isInf = 1;
			}
		} else if ((addition_list[i].left.isInf && !addition_list[i].left.ubit
				&& !addition_list[i].left.sign)
				|| (left_sum.isInf && !left_sum.ubit && !left_sum.sign)) {
			mpz_set_ui(left_sum.fraction, 0);
			left_sum.sign = 0;
			left_sum.ubit = 0;
			left_sum.isInf = 1;
		} else if ((addition_list[i].left.isInf && addition_list[i].left.ubit
				&& addition_list[i].left.sign)
				|| (left_sum.isInf && left_sum.ubit && left_sum.sign)) {
			mpz_set_ui(left_sum.fraction, 0);
			left_sum.sign = 1;
			left_sum.ubit = 1;
			left_sum.isInf = 1;
		}
		mpz_clear(addition_list[i].left.fraction);

		//right endpoint
		if (!addition_list[i].right.isInf && !right_sum.isInf) {
			if (addition_list[i].right.sign)
				mpz_neg(addition_list[i].right.fraction,
						addition_list[i].right.fraction);
			mpz_add(right_sum.fraction, right_sum.fraction,
					addition_list[i].right.fraction);
			right_sum.ubit = right_sum.ubit | addition_list[i].right.ubit;
			if (mpz_sgn(right_sum.fraction) < 0)
				right_sum.sign = 1;
			else
				right_sum.sign = 0;
		} else if ((addition_list[i].right.isInf && !addition_list[i].right.ubit
				&& addition_list[i].right.sign)
				|| (right_sum.isInf && !right_sum.ubit && right_sum.sign)) {
			if ((addition_list[i].right.isInf && !addition_list[i].right.ubit
					&& !addition_list[i].right.sign)
					|| (right_sum.isInf && !right_sum.ubit && !right_sum.sign)) {
				printf("Result is NaN in right endpoint sum\n");
			} else {
				mpz_set_ui(right_sum.fraction, 0);
				right_sum.sign = 1;
				right_sum.ubit = 0;
				right_sum.isInf = 1;
			}
		} else if ((addition_list[i].right.isInf && !addition_list[i].right.ubit
				&& !addition_list[i].right.sign)
				|| (right_sum.isInf && !right_sum.ubit && !right_sum.sign)) {
			mpz_set_ui(right_sum.fraction, 0);
			right_sum.sign = 0;
			right_sum.ubit = 0;
			right_sum.isInf = 1;
		} else if ((addition_list[i].right.isInf && addition_list[i].right.ubit
				&& !addition_list[i].right.sign)
				|| (right_sum.isInf && right_sum.ubit && !right_sum.sign)) {
			mpz_set_ui(right_sum.fraction, 0);
			right_sum.sign = 0;
			right_sum.ubit = 1;
			right_sum.isInf = 1;
		}
		mpz_clear(addition_list[i].right.fraction);
	}

	//convert to ubound
	//left
	int fraction_length = 0, trailing_zeros = 0, ubit = 0;
	unsigned long long added_fractions = 0;
	int truncation_amount = 0;
	if (!left_sum.isInf) {
		//get 64 bits of the fraction and fraction bit length
		if (mpz_sgn(left_sum.fraction) != 0) {
			fraction_length = mpz_sizeinbase(left_sum.fraction, 2);
			trailing_zeros = mpz_scan1(left_sum.fraction, 0);
			if ((fraction_length - trailing_zeros) > 64) {
				truncation_amount = (fraction_length - 64);
				mpz_tdiv_q_2exp(left_sum.fraction, left_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, left_sum.fraction);
				ubit = 1;
				get_unum_for_fused_inter_t(&gbound_result.left_bound,
						added_fractions, ubit, truncation_amount - bias, 64,
						_g_fsizemax, left_sum.sign);
			} else {
				truncation_amount = trailing_zeros;
				mpz_tdiv_q_2exp(left_sum.fraction, left_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, left_sum.fraction);
				ubit = 0;
				get_unum_for_fused_inter_t(&gbound_result.left_bound,
						added_fractions, ubit, truncation_amount - bias,
						fraction_length - trailing_zeros, _g_fsizemax,
						left_sum.sign);
			}
			gbound_result.left_open = left_sum.ubit;
		} else {
			gbound_result.left_bound = _g_zero;
			gbound_result.left_open = left_sum.ubit;
		}
	} else if (left_sum.isInf && left_sum.ubit == 0) {
		gbound_result.left_bound = _g_posinfu;
		gbound_result.left_open = 0;
		if (left_sum.sign)
			gbound_result.left_bound.sign = 1;
	} else {
		gbound_result.left_bound = _g_neginfu;
		gbound_result.left_open = 1;
	}

	//right
	if (!right_sum.isInf) {
		if (mpz_sgn(right_sum.fraction) != 0) {
			fraction_length = mpz_sizeinbase(right_sum.fraction, 2);
			trailing_zeros = mpz_scan1(right_sum.fraction, 0);
			if ((fraction_length - trailing_zeros) > 64) {
				truncation_amount = (fraction_length - 64);
				mpz_tdiv_q_2exp(right_sum.fraction, right_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, right_sum.fraction);
				ubit = 1;
				get_unum_for_fused_inter_t(&gbound_result.right_bound,
						added_fractions, ubit, truncation_amount - bias, 64,
						_g_fsizemax, right_sum.sign);
			} else {
				truncation_amount = trailing_zeros;
				mpz_tdiv_q_2exp(right_sum.fraction, right_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, right_sum.fraction);
				ubit = 0;
				get_unum_for_fused_inter_t(&gbound_result.right_bound,
						added_fractions, ubit, truncation_amount - bias,
						fraction_length - trailing_zeros, _g_fsizemax,
						right_sum.sign);
			}
			gbound_result.right_open = right_sum.ubit;
		} else {
			gbound_result.right_bound = _g_zero;
			gbound_result.right_open = right_sum.ubit;
		}
	} else if (right_sum.isInf && right_sum.ubit == 0) {
		gbound_result.right_bound = _g_posinfu;
		gbound_result.right_open = 0;
		if (right_sum.sign)
			gbound_result.right_bound.sign = 1;
	} else {
		gbound_result.right_bound = _g_posinfu;
		gbound_result.right_open = 1;
	}

	//get ubound result from gbound
	get_ubound_result_from_gbound(result, &gbound_result);

	//release all the previously initialized mpz_t values
	mpz_clear(left_sum.fraction);
	mpz_clear(right_sum.fraction);
	return;

}

void fused_ubound_add(ubound_t* result, int size, ubound_t list_a[]) {
	int i;
	fused_inter_t left_sum, right_sum;
	fused_inter_pair_t addition_list[size];
	gbound_t gbound_a, gbound_result;
	//smallsubnormalu exponent
	int bias = (1 << (_g_esizemax - 1)) - 2;

	//initialize
	mpz_init(left_sum.fraction);
	mpz_init(right_sum.fraction);
	left_sum.isInf = 0;
	right_sum.isInf = 0;
	left_sum.ubit = 0;
	right_sum.ubit = 0;
	left_sum.sign = 0;
	right_sum.sign = 0;

	for (i = 0; i < size; i++) {
		get_gbound_from_ubound(&gbound_a, &list_a[i]);
		get_fused_inter_t_from_unum(&addition_list[i].left,
				gbound_a.left_bound);
		addition_list[i].left.ubit = gbound_a.left_open;
		get_fused_inter_t_from_unum(&addition_list[i].right,
				gbound_a.right_bound);
		addition_list[i].right.ubit = gbound_a.right_open;

		//make the fraction sizes the same
		mpz_mul_2exp(addition_list[i].left.fraction,
				addition_list[i].left.fraction,
				(_g_fsizemax - addition_list[i].left.f_size));
		mpz_mul_2exp(addition_list[i].right.fraction,
				addition_list[i].right.fraction,
				(_g_fsizemax - addition_list[i].right.f_size));

		//make the exponents the same
		mpz_mul_2exp(addition_list[i].left.fraction,
				addition_list[i].left.fraction,
				(addition_list[i].left.exponent + bias));
		mpz_mul_2exp(addition_list[i].right.fraction,
				addition_list[i].right.fraction,
				(addition_list[i].right.exponent + bias));

		//add & release memory of mpz_t
		//left endpoint
		if (!addition_list[i].left.isInf && !left_sum.isInf) {
			if (addition_list[i].left.sign) {
				mpz_neg(addition_list[i].left.fraction,
						addition_list[i].left.fraction);
			}
			mpz_add(left_sum.fraction, left_sum.fraction,
					addition_list[i].left.fraction);
			left_sum.ubit = left_sum.ubit | addition_list[i].left.ubit;
			if (mpz_sgn(left_sum.fraction) < 0) {
				left_sum.sign = 1;
			} else {
				left_sum.sign = 0;
			}
		} else if ((addition_list[i].left.isInf && !addition_list[i].left.ubit
				&& addition_list[i].left.sign)
				|| (left_sum.isInf && !left_sum.ubit && left_sum.sign)) {
			if ((addition_list[i].left.isInf && !addition_list[i].left.ubit
					&& !addition_list[i].left.sign)
					|| (left_sum.isInf && !left_sum.ubit && !left_sum.sign)) {
				printf("Result is NaN in left endpoint sum\n");
			} else {
				mpz_set_ui(left_sum.fraction, 0);
				left_sum.sign = 1;
				left_sum.ubit = 0;
				left_sum.isInf = 1;
			}
		} else if ((addition_list[i].left.isInf && !addition_list[i].left.ubit
				&& !addition_list[i].left.sign)
				|| (left_sum.isInf && !left_sum.ubit && !left_sum.sign)) {
			mpz_set_ui(left_sum.fraction, 0);
			left_sum.sign = 0;
			left_sum.ubit = 0;
			left_sum.isInf = 1;
		} else if ((addition_list[i].left.isInf && addition_list[i].left.ubit
				&& addition_list[i].left.sign)
				|| (left_sum.isInf && left_sum.ubit && left_sum.sign)) {
			mpz_set_ui(left_sum.fraction, 0);
			left_sum.sign = 1;
			left_sum.ubit = 1;
			left_sum.isInf = 1;
		}
		mpz_clear(addition_list[i].left.fraction);

		//right endpoint
		if (!addition_list[i].right.isInf && !right_sum.isInf) {
			if (addition_list[i].right.sign)
				mpz_neg(addition_list[i].right.fraction,
						addition_list[i].right.fraction);
			mpz_add(right_sum.fraction, right_sum.fraction,
					addition_list[i].right.fraction);
			right_sum.ubit = right_sum.ubit | addition_list[i].right.ubit;
			if (mpz_sgn(right_sum.fraction) < 0)
				right_sum.sign = 1;
			else
				right_sum.sign = 0;
		} else if ((addition_list[i].right.isInf && !addition_list[i].right.ubit
				&& addition_list[i].right.sign)
				|| (right_sum.isInf && !right_sum.ubit && right_sum.sign)) {
			if ((addition_list[i].right.isInf && !addition_list[i].right.ubit
					&& !addition_list[i].right.sign)
					|| (right_sum.isInf && !right_sum.ubit && !right_sum.sign)) {
				printf("Result is NaN in right endpoint sum\n");
			} else {
				mpz_set_ui(right_sum.fraction, 0);
				right_sum.sign = 1;
				right_sum.ubit = 0;
				right_sum.isInf = 1;
			}
		} else if ((addition_list[i].right.isInf && !addition_list[i].right.ubit
				&& !addition_list[i].right.sign)
				|| (right_sum.isInf && !right_sum.ubit && !right_sum.sign)) {
			mpz_set_ui(right_sum.fraction, 0);
			right_sum.sign = 0;
			right_sum.ubit = 0;
			right_sum.isInf = 1;
		} else if ((addition_list[i].right.isInf && addition_list[i].right.ubit
				&& !addition_list[i].right.sign)
				|| (right_sum.isInf && right_sum.ubit && !right_sum.sign)) {
			mpz_set_ui(right_sum.fraction, 0);
			right_sum.sign = 0;
			right_sum.ubit = 1;
			right_sum.isInf = 1;
		}
		mpz_clear(addition_list[i].right.fraction);
	}

	//convert to ubound
	//left
	int fraction_length = 0, trailing_zeros = 0, ubit = 0;
	unsigned long long added_fractions = 0;
	int truncation_amount = 0;
	if (!left_sum.isInf) {
		//get 64 bits of the fraction and fraction bit length
		if (mpz_sgn(left_sum.fraction) != 0) {
			fraction_length = mpz_sizeinbase(left_sum.fraction, 2);
			trailing_zeros = mpz_scan1(left_sum.fraction, 0);
			if ((fraction_length - trailing_zeros) > 64) {
				truncation_amount = (fraction_length - 64);
				mpz_tdiv_q_2exp(left_sum.fraction, left_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, left_sum.fraction);
				ubit = 1;
				get_unum_for_fused_inter_t(&gbound_result.left_bound,
						added_fractions, ubit, truncation_amount - bias, 64,
						_g_fsizemax, left_sum.sign);
			} else {
				truncation_amount = trailing_zeros;
				mpz_tdiv_q_2exp(left_sum.fraction, left_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, left_sum.fraction);
				ubit = 0;
				get_unum_for_fused_inter_t(&gbound_result.left_bound,
						added_fractions, ubit, truncation_amount - bias,
						fraction_length - trailing_zeros, _g_fsizemax,
						left_sum.sign);
			}
			gbound_result.left_open = left_sum.ubit;
		} else {
			gbound_result.left_bound = _g_zero;
			gbound_result.left_open = left_sum.ubit;
		}
	} else if (left_sum.isInf && left_sum.ubit == 0) {
		gbound_result.left_bound = _g_posinfu;
		gbound_result.left_open = 0;
		if (left_sum.sign)
			gbound_result.left_bound.sign = 1;
	} else {
		gbound_result.left_bound = _g_neginfu;
		gbound_result.left_open = 1;
	}

	//right
	if (!right_sum.isInf) {
		if (mpz_sgn(right_sum.fraction) != 0) {
			fraction_length = mpz_sizeinbase(right_sum.fraction, 2);
			trailing_zeros = mpz_scan1(right_sum.fraction, 0);
			if ((fraction_length - trailing_zeros) > 64) {
				truncation_amount = (fraction_length - 64);
				mpz_tdiv_q_2exp(right_sum.fraction, right_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, right_sum.fraction);
				ubit = 1;
				get_unum_for_fused_inter_t(&gbound_result.right_bound,
						added_fractions, ubit, truncation_amount - bias, 64,
						_g_fsizemax, right_sum.sign);
			} else {
				truncation_amount = trailing_zeros;
				mpz_tdiv_q_2exp(right_sum.fraction, right_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, right_sum.fraction);
				ubit = 0;
				get_unum_for_fused_inter_t(&gbound_result.right_bound,
						added_fractions, ubit, truncation_amount - bias,
						fraction_length - trailing_zeros, _g_fsizemax,
						right_sum.sign);
			}
			gbound_result.right_open = right_sum.ubit;
		} else {
			gbound_result.right_bound = _g_zero;
			gbound_result.right_open = right_sum.ubit;
		}
	} else if (right_sum.isInf && right_sum.ubit == 0) {
		gbound_result.right_bound = _g_posinfu;
		gbound_result.right_open = 0;
		if (right_sum.sign)
			gbound_result.right_bound.sign = 1;
	} else {
		gbound_result.right_bound = _g_posinfu;
		gbound_result.right_open = 1;
	}

	//get ubound result from gbound
	get_ubound_result_from_gbound(result, &gbound_result);

	//release all the previously initialized mpz_t values
	mpz_clear(left_sum.fraction);
	mpz_clear(right_sum.fraction);
	return;
}

void get_fused_inter_t_from_unum(fused_inter_t* result, unum_t x) {
	unsigned long long op_frac;

	(*result).sign = x.sign;
	(*result).ubit = x.ubit;
	(*result).isInf = isInf(x);
	;
	(*result).f_size = x.f_size + 1;
	if (x.exponent) {
		(*result).exponent = x.exponent - ((1 << x.e_size) - 1);
		op_frac = x.fraction | (1ULL << (x.f_size + 1));
	} else {
		(*result).exponent = 2 - (1 << x.e_size);
		op_frac = x.fraction;
	}
	mpz_init((*result).fraction);
	mpz_import((*result).fraction, 1, -1, sizeof op_frac, 0, 0, &op_frac);
	return;
}

