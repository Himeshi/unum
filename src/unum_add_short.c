#include "unum.h"

void unum_add(unum_t* result, unum_t* op1, unum_t* op2)
{

    unsigned short bias_of_op1, bias_of_op2, length_of_op1, length_of_op2, trailing_zeros;
    int exponent_of_op1, exponent_of_op2, exponent_of_result, fraction_bit_length, smallnormalu_exp, fraction_scale;
    unsigned long long op1_num, op2_num, added_fractions = 0;
    short result_fraction_size, exponent_difference;
    bool result_sign;
    mpz_t mpz_result, mpz_op;

    // if either of the operands are zero
    if (isPosOrNegZero(*op1)) {
        if (isPosOrNegZero(*op2)) {
            (*result) = _g_zero;
        } else {
            (*result) = *op2;
        }
        return;
    }
    if (isPosOrNegZero(*op2)) {
        (*result) = *op1;
        return;
    }

    // ubit
    (*result).ubit = (*op1).ubit | (*op2).ubit;

    // exponent
    bias_of_op1 = (1 << (*op1).e_size) - 1;
    if ((*op1).exponent) {
        exponent_of_op1 = (*op1).exponent - bias_of_op1;
        op1_num = (*op1).fraction | (1ULL << ((*op1).f_size + 1));
    } else {
        exponent_of_op1 = 1 - bias_of_op1;
        op1_num = (*op1).fraction;
    }

    bias_of_op2 = (1 << (*op2).e_size) - 1;
    if ((*op2).exponent) {
        exponent_of_op2 = (*op2).exponent - bias_of_op2;
        op2_num = (*op2).fraction | (1ULL << ((*op2).f_size + 1));
    } else {
        exponent_of_op2 = 1 - bias_of_op2;
        op2_num = (*op2).fraction;
    }

    // align the binary point
    if ((*op1).f_size != (*op2).f_size) {
        if ((*op1).f_size > (*op2).f_size) {
            op2_num <<= ((*op1).f_size - (*op2).f_size);
            result_fraction_size = (*op1).f_size + 1;
        } else {
            op1_num <<= ((*op2).f_size - (*op1).f_size);
            result_fraction_size = (*op2).f_size + 1;
        }
    } else {
        result_fraction_size = (*op1).f_size + 1;
    }

    length_of_op1 = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(op1_num);
    length_of_op2 = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(op2_num);

    mpz_init(mpz_result);
    if ((*op1).sign == (*op2).sign) {
        // sign of result
        result_sign = (*op1).sign;

        // make the exponents the same
        if (exponent_of_op1 != exponent_of_op2) {
            if (exponent_of_op1 > exponent_of_op2) {
                exponent_difference = exponent_of_op1 - exponent_of_op2;
                if ((length_of_op1 + exponent_difference - length_of_op2) >= (_g_fsizemax + 1)) {
                    // in this case the fractions aren't touching, so the addition
                    // does not change the most significant bits
                    // thus, added fraction will be the most significant
                    //_g_fsizemax + 1 bits (no need to check if the fraction scale
                    // will be greater than _g_fsizemax + 1 because aligning the
                    // binary point will not make it greater than that)
                    fraction_scale = (_g_fsizemax + 1 - length_of_op1);
                    added_fractions = op1_num << fraction_scale;
                    exponent_of_result = exponent_of_op1 - fraction_scale;
                    fraction_bit_length = _g_fsizemax + 1;
                    (*result).ubit = 1;
                } else {
                    if (length_of_op1 + exponent_difference < INTERMEDIATE_FRACTION_SIZE) {
                        op1_num <<= exponent_difference;
                        added_fractions = op1_num + op2_num;
                        fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
                    } else {
                        mpz_init(mpz_op);
                        mpz_import(mpz_result, 1, -1, sizeof op1_num, 0, 0, &op1_num);
                        mpz_import(mpz_op, 1, -1, sizeof op2_num, 0, 0, &op2_num);
                        mpz_mul_2exp(mpz_result, mpz_result, exponent_difference);
                        mpz_add(mpz_result, mpz_result, mpz_op);
                        fraction_bit_length = (int)mpz_sizeinbase(mpz_result, 2);
                        mpz_clear(mpz_op);
                    }
                    exponent_of_result = exponent_of_op2;
                }
            } else {
                exponent_difference = exponent_of_op2 - exponent_of_op1;
                if ((length_of_op2 + exponent_difference - length_of_op1) > (_g_fsizemax + 1)) {
                    fraction_scale = (_g_fsizemax + 1 - length_of_op2);
                    added_fractions = op2_num << fraction_scale;
                    exponent_of_result = exponent_of_op2 - fraction_scale;
                    fraction_bit_length = _g_fsizemax + 1;
                    (*result).ubit = 1;
                } else {
                    if (length_of_op1 + exponent_difference < INTERMEDIATE_FRACTION_SIZE) {
                        op2_num <<= exponent_difference;
                        added_fractions = op1_num + op2_num;
                        fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
                    } else {
                        mpz_init(mpz_op);
                        mpz_import(mpz_result, 1, -1, sizeof op1_num, 0, 0, &op2_num);
                        mpz_import(mpz_op, 1, -1, sizeof op2_num, 0, 0, &op1_num);
                        mpz_mul_2exp(mpz_result, mpz_result, exponent_difference);
                        mpz_add(mpz_result, mpz_result, mpz_op);
                        fraction_bit_length = (int)mpz_sizeinbase(mpz_result, 2);
                        mpz_clear(mpz_op);
                    }
                    exponent_of_result = exponent_of_op1;
                }
            }
        } else {
            exponent_of_result = exponent_of_op1;
            added_fractions = op1_num + op2_num;
            fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
        }
    } else {
        if (exponent_of_op1 != exponent_of_op2) {
            if (exponent_of_op1 > exponent_of_op2) {
                exponent_difference = exponent_of_op1 - exponent_of_op2;
                if ((length_of_op1 + exponent_difference - length_of_op2) >= (_g_fsizemax + 1)) {
                    // Again, in this case the fractions aren't touching
                    result_sign = (*op1).sign;
                    if (IS_POWER_OF_2(op1_num)) {
                        // if the fraction is a power of two, the significant bits
                        // will change when 1 is deducted (the most significant bit
                        // will become zero)
                        if (!(length_of_op1 + exponent_difference - length_of_op2 - (_g_fsizemax + 1))) {
                            if (length_of_op1 + exponent_difference <= INTERMEDIATE_FRACTION_SIZE) {
                                op1_num <<= exponent_difference;
                                added_fractions = op1_num - op2_num;
                                fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
                            } else {
                                mpz_init(mpz_op);
                                mpz_import(mpz_result, 1, -1, sizeof op1_num, 0, 0, &op1_num);
                                mpz_import(mpz_op, 1, -1, sizeof op2_num, 0, 0, &op2_num);
                                mpz_mul_2exp(mpz_result, mpz_result, exponent_difference);
                                mpz_sub(mpz_result, mpz_result, mpz_op);
                                fraction_bit_length = (int)mpz_sizeinbase(mpz_result, 2);
                                mpz_clear(mpz_op);
                            }
                            exponent_of_result = exponent_of_op2;
                        } else {
                            added_fractions = (1ULL << (_g_fsizemax + 1)) - 1;
                            // length of result fraction = length_of_op1 + exp_diff - 1
                            // exp_result = exp_of_op2 + (result_fraction - (_g_fsizemax + 1))
                            exponent_of_result =
                                exponent_of_op2 + length_of_op1 + exponent_difference - _g_fsizemax - 2;
                            (*result).ubit = 1;
                            fraction_bit_length = _g_fsizemax + 1;
                        }
                    } else {
                        (*result).ubit = 1;
                        added_fractions = op1_num << (_g_fsizemax + 1 - length_of_op1);
                        added_fractions--;
                        exponent_of_result = exponent_of_op1 - (_g_fsizemax + 1 - length_of_op1);
                        fraction_bit_length = _g_fsizemax + 1;
                    }
                } else {
                    if (length_of_op1 + exponent_difference <= INTERMEDIATE_FRACTION_SIZE) {
                        op1_num <<= exponent_difference;
                        if (op1_num > op2_num) {
                            added_fractions = op1_num - op2_num;
                            result_sign = (*op1).sign;
                        } else {
                            added_fractions = op2_num - op1_num;
                            result_sign = (*op2).sign;
                        }
                        fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
                    } else {
                        mpz_init(mpz_op);
                        mpz_import(mpz_result, 1, -1, sizeof op1_num, 0, 0, &op1_num);
                        mpz_import(mpz_op, 1, -1, sizeof op2_num, 0, 0, &op2_num);
                        mpz_mul_2exp(mpz_result, mpz_result, exponent_difference);
                        mpz_sub(mpz_result, mpz_result, mpz_op);
                        if (mpz_sgn(mpz_result) == 1) {
                            result_sign = (*op1).sign;
                        } else {
                            result_sign = (*op2).sign;
                            mpz_neg(mpz_result, mpz_result);
                        }
                        fraction_bit_length = (int)mpz_sizeinbase(mpz_result, 2);
                        mpz_clear(mpz_op);
                    }
                    exponent_of_result = exponent_of_op2;
                }
            } else {
                exponent_difference = exponent_of_op2 - exponent_of_op1;
                if ((length_of_op2 + exponent_difference - length_of_op1) >= (_g_fsizemax + 1)) {
                    result_sign = (*op2).sign;
                    if (IS_POWER_OF_2(op2_num)) {
                        if (!(length_of_op2 + exponent_difference - length_of_op1 - (_g_fsizemax + 1))) {
                            if (length_of_op2 + exponent_difference <= INTERMEDIATE_FRACTION_SIZE) {
                                op2_num <<= exponent_difference;
                                added_fractions = op2_num - op1_num;
                                fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
                            } else {
                                mpz_init(mpz_op);
                                mpz_import(mpz_result, 1, -1, sizeof op1_num, 0, 0, &op2_num);
                                mpz_import(mpz_op, 1, -1, sizeof op2_num, 0, 0, &op1_num);
                                mpz_mul_2exp(mpz_result, mpz_result, exponent_difference);
                                mpz_sub(mpz_result, mpz_result, mpz_op);
                                fraction_bit_length = (int)mpz_sizeinbase(mpz_result, 2);
                                mpz_clear(mpz_op);
                            }
                            exponent_of_result = exponent_of_op1;
                        } else {
                            added_fractions = (1ULL << (_g_fsizemax + 1)) - 1;
                            exponent_of_result =
                                exponent_of_op1 + length_of_op2 + exponent_difference - _g_fsizemax - 2;
                            (*result).ubit = 1;
                            fraction_bit_length = _g_fsizemax + 1;
                        }
                    } else {
                        (*result).ubit = 1;
                        added_fractions = op2_num << (_g_fsizemax + 1 - length_of_op2);
                        added_fractions--;
                        exponent_of_result = exponent_of_op2 - (_g_fsizemax + 1 - length_of_op2);
                        fraction_bit_length = _g_fsizemax + 1;
                    }
                } else {
                    if (length_of_op2 + exponent_difference <= INTERMEDIATE_FRACTION_SIZE) {
                        op2_num <<= exponent_difference;
                        if (op1_num > op2_num) {
                            added_fractions = op1_num - op2_num;
                            result_sign = (*op1).sign;
                        } else {
                            added_fractions = op2_num - op1_num;
                            result_sign = (*op2).sign;
                        }
                        fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
                    } else {
                        mpz_init(mpz_op);
                        mpz_import(mpz_result, 1, -1, sizeof op1_num, 0, 0, &op2_num);
                        mpz_import(mpz_op, 1, -1, sizeof op2_num, 0, 0, &op1_num);
                        mpz_mul_2exp(mpz_result, mpz_result, exponent_difference);
                        mpz_sub(mpz_result, mpz_result, mpz_op);
                        if (mpz_sgn(mpz_result) == 1) {
                            result_sign = (*op1).sign;
                        } else {
                            result_sign = (*op2).sign;
                            mpz_neg(mpz_result, mpz_result);
                        }
                        fraction_bit_length = (int)mpz_sizeinbase(mpz_result, 2);
                        mpz_clear(mpz_op);
                    }
                    exponent_of_result = exponent_of_op1;
                }
            }
        } else {
            exponent_of_result = exponent_of_op1;
            if (op1_num > op2_num) {
                added_fractions = op1_num - op2_num;
                result_sign = (*op1).sign;
            } else {
                added_fractions = op2_num - op1_num;
                result_sign = (*op2).sign;
            }
            fraction_bit_length = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions);
        }
    }

    // normalize the fraction and get the exponent accordingly
    if (added_fractions) {
        trailing_zeros = __builtin_ctzll(added_fractions);
        if (trailing_zeros > 0 && !(result)->ubit) {
            added_fractions >>= trailing_zeros;
            exponent_of_result += trailing_zeros;
            fraction_bit_length -= trailing_zeros;
            trailing_zeros = 0;
        }

        if (fraction_bit_length > (_g_fsizemax + 1)) {
            // first adjust the numerator of the fraction
            added_fractions >>= (fraction_bit_length - _g_fsizemax - 1);
            exponent_of_result += (fraction_bit_length - _g_fsizemax - 1);
            fraction_bit_length = _g_fsizemax + 1;
            (*result).ubit = 1;
            // next adjust the denominator of the fraction
            exponent_of_result += (_g_fsizemax - result_fraction_size);
            result_fraction_size = _g_fsizemax;
        } else {
            // in this case we only need to adjust the denominator
            exponent_of_result += (fraction_bit_length - result_fraction_size - 1);
            result_fraction_size = fraction_bit_length - 1;
        }
    } else if (mpz_sgn(mpz_result)) {
        trailing_zeros = mpz_scan1(mpz_result, 0);
        if (trailing_zeros > 0 && !(result)->ubit) {
            mpz_tdiv_q_2exp(mpz_result, mpz_result, trailing_zeros);
            exponent_of_result += trailing_zeros;
            fraction_bit_length -= trailing_zeros;
            trailing_zeros = 0;
        }

        if (fraction_bit_length > (_g_fsizemax + 1)) {
            mpz_tdiv_q_2exp(mpz_result, mpz_result, (fraction_bit_length - _g_fsizemax - 1));
            exponent_of_result += (fraction_bit_length - _g_fsizemax - 1);
            fraction_bit_length = _g_fsizemax + 1;
            (*result).ubit = 1;
            exponent_of_result += (_g_fsizemax - result_fraction_size);
            result_fraction_size = _g_fsizemax;
        } else {
            exponent_of_result += (fraction_bit_length - result_fraction_size - 1);
            result_fraction_size = fraction_bit_length - 1;
        }
        mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0, 0, mpz_result);
    } else {
        (*result) = _g_zero;
        return;
    }
    mpz_clear(mpz_result);

    // result is greater than what can be expressed
    //(an example of why is the extra check is needed: 1 + 6 in {1,1})
    if (exponent_of_result > _g_maxexp || (exponent_of_result == _g_maxexp && added_fractions >= _g_maxfracvalhidden)) {
        (*result) = _g_maxrealu;
        // when the answer is inexact maxrealu, a bit from the fraction can be
        // thrown out
        if (_g_fsizesize > 0) {
            (*result).fraction >>= 1;
            (*result).f_size -= 1;
        }
        (*result).sign = result_sign;
        (*result).ubit = true;
        return;
    }

    // result is smaller than what can be expressed
    // no need to check for fractions with _g_minexp since the minimum fraction
    // is that of _g_smallsubnormalu so there cannot be a number with _g_minexp
    // as exponent and a fraction less than the fraction of _g_smallsubnormalu
    if (exponent_of_result < _g_minexp) {
        (*result) = _g_smallsubnormalu;
        (*result).fraction--;
        (*result).sign = result_sign;
        (*result).ubit = true;
        return;
    }

    // if the result is subnormal
    // if the exponent is less than exponent of smallnormalu, then the result is subnormal
    // smallnormalu_exp = 1 - (pow(2, (_g_esizemax - 1)) - 1);
    smallnormalu_exp = 1 - ((1 << (_g_esizemax - 1)) - 1);
    if (exponent_of_result < smallnormalu_exp) {
        (*result).exponent = 0;
        (*result).e_size = _g_esizemax - 1;
        (*result).sign = result_sign;

        // fix the exponent to 1-bias and calculate the bits of the fraction
        fraction_scale = smallnormalu_exp - exponent_of_result;
        fraction_bit_length = result_fraction_size + fraction_scale;

        // set the fraction
        if (fraction_bit_length > _g_fsizemax) {
            // if result is larger than fsizemax and/or is inexact
            (*result).ubit = 1;
            // shift until it fits _g_fsizemax bits
            (*result).fraction = added_fractions >> (fraction_bit_length - _g_fsizemax);
            (*result).f_size = _g_fsizemax - 1;
        } else {
            (*result).fraction = added_fractions;
            (*result).f_size = fraction_bit_length - 1;
        }
        return;
    }

    // handling normal numbers

    // sign
    (*result).sign = result_sign;

    // reset the leading bit of the multiplied fraction (hidden bit))
    added_fractions &= ~(1ULL << (INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(added_fractions) - 1));
    fraction_bit_length = result_fraction_size; // this length already accounts for the hidden 1

    // set the fraction
    if (added_fractions == 0 && (*result).ubit == 0) {
        (*result).fraction = 0;
        (*result).f_size = 0;
    } else {
        (*result).fraction = added_fractions;
        (*result).f_size = fraction_bit_length - 1;
    }

    // exponent
    if (!exponent_of_result && !(*result).fraction && !(*result).ubit) {
        (*result).fraction = 1;
        (*result).f_size = 0;
        (*result).e_size = 0;
        (*result).exponent = 0;
        return;
    } else if (exponent_of_result == 1) {
        (*result).e_size = 0;
        (*result).exponent = 1;
    } else {
        (*result).e_size = INTERMEDIATE_EXP_SIZE - __builtin_clz(abs(exponent_of_result - 1));
        //(*result).exponent = exponent_of_result + (pow(2, ((*result).e_size)) - 1);
        (*result).exponent = exponent_of_result + ((1 << (*result).e_size) - 1);
    }

    // in case the value is bordering infinity, but shouldn't
    // if (((*result).exponent != _g_maxexpval || (*result).fraction != (_g_maxfracval - 1)) && (*result).exponent ==
    // (pow(2, ((*result).e_size + 1)) - 1) && (*result).fraction == (pow(2, ((*result).f_size + 1)) - 1) &&
    // (*result).ubit) {
    if (((*result).exponent != _g_maxexpval || (*result).fraction != (_g_maxfracval - 1)) &&
        (*result).exponent == ((1 << ((*result).e_size + 1)) - 1) &&
        (*result).fraction == ((1ULL << ((*result).f_size + 1)) - 1) && (*result).ubit) {
        // if fraction can be increased, increase that
        if ((*result).f_size < (_g_fsizemax - 1)) {
            (*result).f_size++;
            (*result).fraction = (*result).fraction << 1;
        } else {
            (*result).e_size++;
            //(*result).exponent = exponent_of_result + (pow(2, ((*result).e_size)) - 1);
            (*result).exponent = exponent_of_result + ((1 << (*result).e_size) - 1);
        }
    }

    // when the answer is inexact maxrealu, remove the bit from the fraction
    if (isPosOrNegInexactMaxreal((*result)) && _g_fsizesize > 0) {
        (*result).fraction >>= 1;
        (*result).f_size -= 1;
    }
    return;
}

void plusg(gbound_t* result, gbound_t op1, gbound_t op2)
{

    if (isNaN(op1.left_bound) || isNaN(op1.right_bound) || isNaN(op2.left_bound) || isNaN(op2.right_bound)) {
        (*result).left_bound = _g_qNaNu;
        (*result).left_open = 1;
        (*result).right_bound = _g_qNaNu;
        (*result).right_open = 1;
        return;
    }

    // sumleft
    if (!isInf(op1.left_bound) && !isInf(op2.left_bound)) {
        unum_add(&(*result).left_bound, &(op1.left_bound), &(op2.left_bound));
        (*result).left_open = op1.left_open || op2.left_open;
    } else if (isNegInf(op1.left_bound) && !op1.left_open) {
        if (isPosInf(op2.left_bound) && !op2.left_open) {
            (*result).left_bound = _g_qNaNu;
            (*result).left_open = 1;
        } else {
            (*result).left_bound = _g_neginfu;
            (*result).left_open = 0;
        }
    } else if (isNegInf(op2.left_bound) && !op2.left_open) {
        if (isPosInf(op1.left_bound) && !op1.left_open) {
            (*result).left_bound = _g_qNaNu;
            (*result).left_open = 1;
        } else {
            (*result).left_bound = _g_neginfu;
            (*result).left_open = 0;
        }
    } else if ((isPosInf(op1.left_bound) && !op1.left_open) || (isPosInf(op2.left_bound) && !op2.left_open)) {
        (*result).left_bound = _g_posinfu;
        (*result).left_open = 0;
    } else if (isNegInf(op1.left_bound)) {
        if (isPosInf(op2.left_bound) && !op2.left_open) {
            (*result).left_bound = _g_posinfu;
            (*result).left_open = 0;
        } else {
            (*result).left_bound = _g_neginfu;
            (*result).left_open = 1;
        }
    } else if (isNegInf(op2.left_bound)) {
        if (isPosInf(op1.left_bound) && !op1.left_open) {
            (*result).left_bound = _g_posinfu;
            (*result).left_open = 0;
        } else {
            (*result).left_bound = _g_neginfu;
            (*result).left_open = 1;
        }
    }

    if (!isInf(op1.right_bound) && !isInf(op2.right_bound)) {
        unum_add(&(*result).right_bound, &(op1.right_bound), &(op2.right_bound));
        (*result).right_open = op1.right_open || op2.right_open;
    } else if (isNegInf(op1.right_bound) && !op1.right_open) {
        if (isPosInf(op2.right_bound) && !op2.right_open) {
            (*result).right_bound = _g_qNaNu;
            (*result).right_open = 1;
        } else {
            (*result).right_bound = _g_neginfu;
            (*result).right_open = 0;
        }
    } else if (isNegInf(op2.right_bound) && !op2.right_open) {
        if (isPosInf(op1.right_bound) && !op1.right_open) {
            (*result).right_bound = _g_qNaNu;
            (*result).right_open = 1;
        } else {
            (*result).right_bound = _g_neginfu;
            (*result).right_open = 0;
        }
    } else if ((isPosInf(op1.right_bound) && !op1.right_open) || (isPosInf(op2.right_bound) && !op2.right_open)) {
        (*result).right_bound = _g_posinfu;
        (*result).right_open = 0;
    } else if (isPosInf(op1.right_bound)) {
        if (isNegInf(op2.right_bound) && !op2.right_open) {
            (*result).right_bound = _g_neginfu;
            (*result).right_open = 0;
        } else {
            (*result).right_bound = _g_posinfu;
            (*result).right_open = 1;
        }
    } else if (isPosInf(op2.right_bound)) {
        if (isNegInf(op1.right_bound) && !op1.right_open) {
            (*result).right_bound = _g_neginfu;
            (*result).right_open = 0;
        } else {
            (*result).right_bound = _g_posinfu;
            (*result).right_open = 1;
        }
    }

    return;
}

void plusu(ubound_t* result, unum_t op1, unum_t op2)
{
#ifdef BIT_TRACK
    int op1_bits, op2_bits;
    op1_bits = 1 + op1.e_size + 1 + op1.f_size + 1 + _g_utagsize;
    op2_bits = 1 + op2.e_size + 1 + op2.f_size + 1 + _g_utagsize;
    total_bit_count += (op1_bits + op2_bits);
    total_op_count += 2;
    if (op1_bits > max_bit_count)
        max_bit_count = op1_bits;
    if (op2_bits > max_bit_count)
        max_bit_count = op2_bits;
    if (op1_bits < min_bit_count)
        min_bit_count = op1_bits;
    if (op2_bits < min_bit_count)
        min_bit_count = op2_bits;
#endif

    gbound_t gbound_op1, gbound_op2, gbound_result;

    // if both numbers are exact, add and return
    if (op1.ubit == 0 && op2.ubit == 0) {
        if (!isNaNOrInf(op1) && !isNaNOrInf(op2) && !isPosOrNegZero(op1) && !isPosOrNegZero(op2)) {
            unum_add(&(*result).left_bound, &op1, &op2);
            (*result).right_bound = (*result).left_bound;
#ifdef BIT_TRACK
            int bits = 1 + (*result).left_bound.e_size + 1 + (*result).left_bound.f_size + 1 + _g_utagsize;
            total_bit_count += bits;
            total_op_count += 1;
            if (bits > max_bit_count)
                max_bit_count = bits;
            if (bits < min_bit_count)
                min_bit_count = bits;
#endif
            return;
        }

        if (isPosOrNegZero(op1)) {
            if (isPosOrNegZero(op2)) {
                (*result).left_bound = _g_zero;
                (*result).right_bound = _g_zero;
#ifdef BIT_TRACK
                int bits = 3 + _g_utagsize;
                total_bit_count += bits;
                total_op_count += 1;
                if (bits > max_bit_count)
                    max_bit_count = bits;
                if (bits < min_bit_count)
                    min_bit_count = bits;
#endif
                return;
            } else {
                (*result).left_bound = op2;
                (*result).right_bound = op2;
#ifdef BIT_TRACK
                int bits = 1 + op2.e_size + 1 + op2.f_size + 1 + _g_utagsize;
                total_bit_count += bits;
                total_op_count += 1;
                if (bits > max_bit_count)
                    max_bit_count = bits;
                if (bits < min_bit_count)
                    min_bit_count = bits;
#endif
                return;
            }
        }

        if (isPosOrNegZero(op2)) {
            (*result).left_bound = op1;
            (*result).right_bound = op1;
#ifdef BIT_TRACK
            int bits = 1 + op1.e_size + 1 + op1.f_size + 1 + _g_utagsize;
            total_bit_count += bits;
            total_op_count += 1;
            if (bits > max_bit_count)
                max_bit_count = bits;
            if (bits < min_bit_count)
                min_bit_count = bits;
#endif
            return;
        }

        if (isNegInf(op1)) {
            if (isPosInf(op2)) {
                (*result).left_bound = _g_qNaNu;
                (*result).right_bound = _g_qNaNu;
                return;
            } else {
                (*result).left_bound = _g_neginfu;
                (*result).right_bound = _g_neginfu;
#ifdef BIT_TRACK
                int bits = _g_maxubits;
                total_bit_count += bits;
                total_op_count += 1;
                if (bits > max_bit_count)
                    max_bit_count = bits;
                if (bits < min_bit_count)
                    min_bit_count = bits;
#endif
            }
            return;
        }

        if (isNegInf(op2)) {
            if (isPosInf(op1)) {
                (*result).left_bound = _g_qNaNu;
                (*result).right_bound = _g_qNaNu;
                return;
            } else {
                (*result).left_bound = _g_neginfu;
                (*result).right_bound = _g_neginfu;
#ifdef BIT_TRACK
                int bits = _g_maxubits;
                total_bit_count += bits;
                total_op_count += 1;
                if (bits > max_bit_count)
                    max_bit_count = bits;
                if (bits < min_bit_count)
                    min_bit_count = bits;
#endif
            }
            return;
        }

        if (isPosInf(op1) || isPosInf(op2)) {
            (*result).left_bound = _g_posinfu;
            (*result).right_bound = _g_posinfu;
#ifdef BIT_TRACK
            int bits = _g_maxubits;
            total_bit_count += bits;
            total_op_count += 1;
            if (bits > max_bit_count)
                max_bit_count = bits;
            if (bits < min_bit_count)
                min_bit_count = bits;
#endif
            return;
        }
    }

    // if either of the unums are inexact, then extract the bounds and divide
    get_gbound_from_unum(&gbound_op1, &op1);
    get_gbound_from_unum(&gbound_op2, &op2);

    if (isNaN(gbound_op1.left_bound) || isNaN(gbound_op2.left_bound) || isNaN(gbound_op1.right_bound) ||
        isNaN(gbound_op2.right_bound)) {
        result->left_bound = _g_qNaNu;
        result->right_bound = _g_qNaNu;
        return;
    }

    // multiply
    plusg(&gbound_result, gbound_op1, gbound_op2);

    // if the result contains NaNs
    if (isNaN(gbound_result.left_bound) || isNaN(gbound_result.right_bound)) {
        result->left_bound = _g_qNaNu;
        result->right_bound = _g_qNaNu;
        return;
    }

    get_ubound_result_from_gbound(result, &gbound_result);

#ifdef BIT_TRACK
    int bits;
    bits = 3 + result->left_bound.e_size + result->left_bound.f_size + _g_utagsize;
    if (!unum_compare(result->left_bound, result->right_bound))
        bits += (3 + result->right_bound.e_size + result->right_bound.f_size + _g_utagsize);
    total_bit_count += bits;
    total_op_count += 1;
    if (bits > max_bit_count)
        max_bit_count = bits;
    if (bits < min_bit_count)
        min_bit_count = bits;
#endif

    return;
}

void plusubound(ubound_t* result, ubound_t op1, ubound_t op2)
{
    gbound_t gbound_op1, gbound_op2, gbound_result;

#ifdef BIT_TRACK
    int bits;
    bits = 3 + op1.left_bound.e_size + op1.left_bound.f_size + _g_utagsize;
    if (!unum_compare(op1.left_bound, op1.right_bound))
        bits += (3 + op1.right_bound.e_size + op1.right_bound.f_size + _g_utagsize);
    total_bit_count += bits;
    total_op_count += 1;
    if (bits > max_bit_count)
        max_bit_count = bits;
    if (bits < min_bit_count)
        min_bit_count = bits;

    bits = 3 + op2.left_bound.e_size + op2.left_bound.f_size + _g_utagsize;
    if (!unum_compare(op2.left_bound, op2.right_bound))
        bits += (3 + op2.right_bound.e_size + op2.right_bound.f_size + _g_utagsize);
    total_bit_count += bits;
    total_op_count += 1;
    if (bits > max_bit_count)
        max_bit_count = bits;
    if (bits < min_bit_count)
        min_bit_count = bits;
#endif

    get_gbound_from_ubound(&gbound_op1, &op1);
    get_gbound_from_ubound(&gbound_op2, &op2);

    if (isNaN(gbound_op1.left_bound) || isNaN(gbound_op2.left_bound) || isNaN(gbound_op1.right_bound) ||
        isNaN(gbound_op2.right_bound)) {
        result->left_bound = _g_qNaNu;
        result->right_bound = _g_qNaNu;
        return;
    }

    // if the left and right bounds are the same in both operands, use the plusu function
    if (unum_compare(gbound_op1.left_bound, gbound_op1.right_bound) &&
        unum_compare(gbound_op2.left_bound, gbound_op2.right_bound)) {
        plusu(result, gbound_op1.left_bound, gbound_op2.left_bound);
#ifdef BIT_TRACK
        bits = 3 + op1.left_bound.e_size + op1.left_bound.f_size + _g_utagsize;
        total_bit_count -= bits;
        total_op_count -= 1;
        bits = 3 + op2.left_bound.e_size + op2.left_bound.f_size + _g_utagsize;
        total_bit_count -= bits;
        total_op_count -= 1;
#endif
        return;
    }

    // divide
    plusg(&gbound_result, gbound_op1, gbound_op2);

    // if the result contains NaNs
    if (isNaN(gbound_result.left_bound) || isNaN(gbound_result.right_bound)) {
        result->left_bound = _g_qNaNu;
        result->right_bound = _g_qNaNu;
        return;
    }

    get_ubound_result_from_gbound(result, &gbound_result);

#ifdef BIT_TRACK
    bits = 3 + result->left_bound.e_size + result->left_bound.f_size + _g_utagsize;
    if (!unum_compare(result->left_bound, result->right_bound))
        bits += (3 + result->right_bound.e_size + result->right_bound.f_size + _g_utagsize);
    total_bit_count += bits;
    total_op_count += 1;
    if (bits > max_bit_count)
        max_bit_count = bits;
    if (bits < min_bit_count)
        min_bit_count = bits;
#endif

    return;
}
