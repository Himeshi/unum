/*
 * Includes common functions for testing unums
 * Compile with gcc -lm -lunum -c unum_test_util.c
 */

#include "unum_test.h"

void get_ubound_string_for_test(char* ubound_string, ubound_t x) {
	char left[1000], right[1000];
	ubound_string[0] = '\0';
	get_unum_string_for_test(left, x.left_bound);
	get_unum_string_for_test(right, x.right_bound);
	strcat(ubound_string, left);
	strcat(ubound_string, ",");
	strcat(ubound_string, right);
	return;
}

void get_unum_string_for_test(char* unum_string, unum_t x) {
	char temp[1000];

	sprintf(unum_string, "%d", x.sign);
	get_bit_string_for_test(temp, x.exponent, (x.e_size + 1));
	strcat(unum_string, temp);
	get_bit_string_for_test(temp, x.fraction, (x.f_size + 1));
	strcat(unum_string, temp);
	sprintf(temp, "%d", x.ubit);
	strcat(unum_string, temp);
	get_bit_string_for_test(temp, x.e_size, _g_esizesize);
	strcat(unum_string, temp);
	get_bit_string_for_test(temp, x.f_size, _g_fsizesize);
	strcat(unum_string, temp);
	return;
}


void get_bit_string_for_test(char* buffer, unsigned long long x, int width) {
	int size, i, leading_zeros = 0;
	unsigned long long bitmask;
	char temp[1000];
	buffer[0] = '\0';

	if (width == 0)
		return;

	if (x == 0) {
		for (i = 0; i < width; i++) {
			strcpy(temp, "0");
			strcat(buffer, temp);
		}
		return;
	} else {
		size = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(x) - 1;
		leading_zeros = __builtin_clzll(x)
				- (INTERMEDIATE_FRACTION_SIZE - width);
		for (i = 0; i < leading_zeros; i++) {
			strcpy(temp, "0");
			strcat(buffer, temp);
		}
	}

	for (i = size; i >= 0; i--) {
		bitmask = 1ULL << i;
		if (x & bitmask) {
			strcpy(temp, "1");
			strcat(buffer, temp);
		} else {
			strcpy(temp, "0");
			strcat(buffer, temp);
		}
	}
	return;
}

