/*Sample program to test unum.h
 * Insert tests, compile with gcc unum_test_main.c unum_test_util.c unum_common_short_test.c unum_mul_short_test.c
 * unum_div_short_test.c unum_sqrt_short_test.c unum_add_short_test.c unum_sub_short_test.c unum_perf_test.c unum_fused_dot_product_test.c -lunum
 * -lgmp -lmpfr -lm -o unum_test
 * Run with ./unum_test
 */

#include <stdio.h>
#include "unum_test.h"

int main()
{
    // IMPORTANT NOTE: test cases for timesu test the result against the
    // mathematica prototype which always use unification. Therefore
    // to run these tests successfully the unum library needs to be built
    // using "make UNIFY=1" (by default unification is turned off)
    // test_timesu_all_envs();

    test_x2u_all_envs();

    // test_mul_perf();

    test_sqrtu_all_envs();

    test_divideu_all_envs();

    test_plusu_all_envs();

    test_timesu_all_envs();

    test_minusu_all_envs();

    test_fused_dot_product();

    test_fused_dot_product2();

    test_fused_multiply_add();

    test_fused_multiply_add2();

    test_fused_multiply_add3();

    return 0;
}
