#include "unum_test.h"

void test_fused_dot_product() {
	unum_t x[4];
	unum_t y[4];
	ubound_t result;

	printf("Testing fused dot product...\n");

	set_env(2, 2);

	x2u(-0.94393130761754295843246609365451149642467498779296875, &x[0]);
	x2u(-0.6300903864345002158842135031591169536113739013671875, &x[1]);
	x2u(0.1166204768776057942858415117370896041393280029296875, &x[2]);
	x2u(0.6813549565530172547056508847163058817386627197265625, &x[3]);

	x2u(-0.9102367627947762773743534125969745218753814697265625, &y[0]);
	x2u(0.9373031062713372829620084303314797580242156982421875, &y[1]);
	x2u(0.60521527454499857157088626991026103496551513671875, &y[2]);
	x2u(-0.86818284907759302182483907017740420997142791748046875, &y[3]);

	fused_unum_dot_product(&result, 4, x, y);
	uboundview(result);
}

void test_fused_dot_product2() {
	unum_t x[4];
	unum_t y[4];
	ubound_t result;

	printf("Testing fused dot product 2...\n");

	set_env(3, 0);

	x2u(4096, &x[0]);
	x2u(-1, &x[1]);
	x2u(1, &x[2]);
	x2u(2048, &x[3]);

	x2u(4096, &y[0]);
	x2u(-1, &y[1]);
	x2u(1, &y[2]);
	x2u(-8192, &y[3]);

	fused_unum_dot_product(&result, 4, x, y);
	uboundview(result);
}

void test_fused_multiply_add() {
	ubound_t a, b, c, result;

	printf("Testing fused multiply add...\n");

	set_env(3, 5);

	x2ub(-0.94393130761754295843246609365451149642467498779296875, &a);
	x2ub(-0.6300903864345002158842135031591169536113739013671875, &b);
	x2ub(0.1166204768776057942858415117370896041393280029296875, &c);

	uboundview(a);
	uboundview(b);
	uboundview(c);

	timesubound(&result, b, c);
	plusubound(&result, result, a);

	ubound_fused_multiply_add(&a, b, c);
	uboundview(a);
	printf("Actual answer:\n");
	uboundview(result);
	printf("Double answer: %f\n",
			-0.6300903864345002158842135031591169536113739013671875
					* 0.1166204768776057942858415117370896041393280029296875
					- 0.94393130761754295843246609365451149642467498779296875);
}

void test_fused_multiply_add2() {
	ubound_t a, b, c, result;

	printf("Testing fused multiply add...\n");

	set_env(3, 5);

	x2ub(9, &a);
	x2ub(6, &b);
	x2ub(11, &c);

	uboundview(a);
	uboundview(b);
	uboundview(c);

	timesubound(&result, b, c);
	plusubound(&result, result, a);

	ubound_fused_multiply_add(&a, b, c);
	uboundview(a);
	printf("Actual answer:\n");
	uboundview(result);
}

void test_fused_multiply_add3() {
	ubound_t a, b, c, result;

	printf("Testing fused multiply add...\n");

	set_env(3, 5);

	x2ub(0.1, &a);
	x2ub(0.2, &b);
	x2ub(0.3, &c);

	uboundview(a);
	uboundview(b);
	uboundview(c);

	timesubound(&result, b, c);
	plusubound(&result, result, a);

	ubound_fused_multiply_add(&a, b, c);
	uboundview(a);
	printf("Actual answer:\n");
	uboundview(result);
}
