/*Performance tests for unum.h*/
#include "unum_test.h"
#include <libunum.h>

union FLT
{
    float f;
    unsigned long i;
};

union DBL
{
    double d;
    unsigned long long li;
};

void test_mul_perf()
{

    int i, j, k;
    float a, b, c, d;
    double da, db, dc, dd;
    double volatile t1, t2, loop, t3, clk;
    mpf_t mpf_a, mpf_b, mpf_c;
    unum_t unum_a, unum_b, unum_c;
    ubound_t op1, op2, op3;
    struct timespec ts0_start, ts0_end;
    struct timespec ts1_start, ts1_end;
    struct timespec ts2_start, ts2_end;
    struct timespec ts3_start, ts3_end;
    struct timespec ts4_start, ts4_end;

    const union FLT ia = { .i = 0x41200069 };           // a = 10.000001
    const union DBL ida = { .li = 0x4024000d20000000 }; // a = 10.000001

    const union FLT ib = { .i = 0x3f80068e };           // a = 1.0002
    const union DBL idb = { .li = 0x3ff000d1c0000000 }; // a = 1.0002

    const union FLT ic = { .i = 0x3f7ff2e5 };           // c = 1/b = 0.9998
    const union DBL idc = { .li = 0x3feffe5ca0000000 }; //

    const union FLT id = { .i = 0xbf80068e };           // a = 1.0002
    const union DBL idd = { .li = 0xbff000d1c0000000 }; // a = 1.0002

    int volatile x;

    int rep1 = 100;
    int rep2 = 1000;

    for (k = 0; k < 10; k++) {
        for (j = 0; j < rep2; j++) {
            for (i = 0; i < rep1; i++) {
                x++;
            }
            x++;
        }
    }

    ts0_start.tv_sec = ts0_start.tv_nsec = ts0_end.tv_sec = ts0_end.tv_nsec = 0;
    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts0_start) != 0)
        perror("gettime error");

    for (k = 0; k < 10; k++) {
        for (j = 0; j < rep2; j++) {
            for (i = 0; i < rep1; i++) {
                x++;
            }
            x++;
        }
    }

    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts0_end) != 0)
        perror("gettime error");

    t1 = (double)(ts0_start.tv_sec) + ((double)(ts0_start.tv_nsec) * 1e-9);
    t2 = (double)(ts0_end.tv_sec) + ((double)(ts0_end.tv_nsec) * 1e-9);
    loop = (t2 - t1) / 10.0;

    printf("loop = %lf\n", loop);
    fflush(stdout);

    /***************************************************/

    a = ia.f;
    b = ib.f;
    c = ic.f;
    d = id.f;

    ts2_start.tv_sec = ts2_start.tv_nsec = ts2_end.tv_sec = ts2_end.tv_nsec = 0;
    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts2_start) != 0)
        perror("gettime error");

    for (j = 0; j < rep2; j++) {
        for (i = 0; i < rep1; i++) {
            a = a * b;
            a = a * c;
            x++;
        }
        x++;
    }

    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts2_end) != 0)
        perror("gettime error");

    t1 = (double)(ts2_start.tv_sec) + ((double)(ts2_start.tv_nsec) * 1e-9);
    t2 = (double)(ts2_end.tv_sec) + ((double)(ts2_end.tv_nsec) * 1e-9);

    t3 = (t2 - t1);

    printf("mulss (single precision mul) time = %lf (without loop = %lf)\n", t3, t3 - loop);
    printf("a = %f\n", a);
    fflush(stdout);

    /***************************************************/

    da = ida.d;
    db = idb.d;
    dc = idc.d;
    dd = idd.d;

    ts4_start.tv_sec = ts4_start.tv_nsec = ts4_end.tv_sec = ts4_end.tv_nsec = 0;
    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts4_start) != 0)
        perror("gettime error");

    for (j = 0; j < rep2; j++) {
        for (i = 0; i < rep1; i++) {
            da = da * db;
            da = da * dc;
            x++;
        }
        x++;
    }

    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts4_end) != 0)
        perror("gettime error");

    t1 = (double)(ts4_start.tv_sec) + ((double)(ts4_start.tv_nsec) * 1e-9);
    t2 = (double)(ts4_end.tv_sec) + ((double)(ts4_end.tv_nsec) * 1e-9);
    t3 = t2 - t1;

    printf("mulsd (double precision mul) time = %lf (without loop = %lf)\n", t3, t3 - loop);
    printf("da = %lf\n", da);

    /***************************************************/

    mpf_init_set_d(mpf_a, 10.000001);
    mpf_init_set_d(mpf_b, 1.0002);
    mpf_init_set_d(mpf_c, 0.9998);

    ts4_start.tv_sec = ts4_start.tv_nsec = ts4_end.tv_sec = ts4_end.tv_nsec = 0;
    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts4_start) != 0)
        perror("gettime error");

    for (j = 0; j < rep2; j++) {
        for (i = 0; i < rep1; i++) {
            mpf_mul(mpf_a, mpf_a, mpf_b);
            mpf_mul(mpf_a, mpf_a, mpf_c);
            x++;
        }
        x++;
    }

    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts4_end) != 0)
        perror("gettime error");

    t1 = (double)(ts4_start.tv_sec) + ((double)(ts4_start.tv_nsec) * 1e-9);
    t2 = (double)(ts4_end.tv_sec) + ((double)(ts4_end.tv_nsec) * 1e-9);
    t3 = t2 - t1;

    printf("mpf time = %lf (without loop = %lf)\n", t3, t3 - loop);
    gmp_printf("mpf_a = %.Ff \n", mpf_a);

    /***************************************************/

    set_env(3, 4);

    x2u(10.000001, &unum_a);
    x2u(1.0002, &unum_b);
    x2u(0.9998, &unum_c);

    op1.left_bound = unum_a;
    op1.right_bound = unum_a;
    op2.left_bound = unum_b;
    op2.right_bound = unum_b;
    op3.left_bound = unum_c;
    op3.right_bound = unum_c;

    ts4_start.tv_sec = ts4_start.tv_nsec = ts4_end.tv_sec = ts4_end.tv_nsec = 0;
    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts4_start) != 0)
        perror("gettime error");

    for (j = 0; j < rep2; j++) {
        for (i = 0; i < rep1; i++) {
            timesubound(&op1, op1, op2);
            timesubound(&op1, op1, op3);
            uboundview(op1);
            printf("\n");
            x++;
        }
        x++;
    }

    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts4_end) != 0)
        perror("gettime error");

    t1 = (double)(ts4_start.tv_sec) + ((double)(ts4_start.tv_nsec) * 1e-9);
    t2 = (double)(ts4_end.tv_sec) + ((double)(ts4_end.tv_nsec) * 1e-9);
    t3 = t2 - t1;

    printf("unum time = %lf (without loop = %lf)\nunum_a = ", t3, t3 - loop);
    uboundview(op1);
    printf("\n");

    /*ubound_t ua, ub, uc;
int i, j, k, repeat = 100000000;
long double time_taken_float, time_taken_unum, time_taken_double, time_taken_mpf, loop_seconds;
float a, b, c;
double da, db, dc;
clock_t start_l, end_l, start_f, end_f, start_u, end_u, start_d, end_d, start_mpf, end_mpf;
float input[12] = {0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10.0, 100.0, 1000.0, 10000.0, 1000000.0};
ubound_t result;
mpf_t mpf_a, mpf_b, mpf_c;

set_env(3,4);

printf("Repeat factor: %d\n",repeat);

/*for (i = 0; i < 12; i++) {

    printf("input: %f |", input[i]);*/

    // loop time
    /* k = 0;
     start_l = clock();
     for (j = 0; j < repeat; j++) {
                     k += rand();
     }
     end_l = clock();
     loop_seconds = ((long double)(end_l - start_l)) / CLOCKS_PER_SEC;
     //printf("k is %d |", k);
     printf("loop time: %Lf |",loop_seconds);

     //float calculation
     k = 0;
             a = 10.0001;
             b = 1.0002;
             c = 0.9998;
     start_f = clock();
     a = input[i];
     for (j = 0; j < repeat; j++) {
                     a *= b;
                     a *= c;
                     k += rand();
     }
     end_f = clock();
     time_taken_float = (((long double)(end_f - start_f)) / CLOCKS_PER_SEC) - loop_seconds;
     printf("k is %d |", k);
     printf("float answer: %e |", a);
     printf("float time: %Lf |",time_taken_float);

     //double calculation
     k=0;
     da = 10.0001;
             db = 1.0002;
             dc = 0.9998;
     double temp = input[i];
     start_d = clock();
     for (j = 0; j < repeat; j++) {
         da *= db;
                     da *= dc;
                     k += rand();
     }
     end_d = clock();
     time_taken_double = (((long double)(end_d - start_d)) / CLOCKS_PER_SEC) - loop_seconds;
     printf("k is %d |", k);
     printf("double answer: %e |", da);
     printf("double time: %Lf |",time_taken_double);

    //gmp calculation
     k=0;
     mpf_init_set_d(mpf_a, 10.0001);
     mpf_init_set_d(mpf_b, 1.0002);
     mpf_init_set_d(mpf_c, 0.9998);
     start_mpf = clock();
     for (j = 0; j < repeat; j++) {
         mpf_mul(mpf_a, mpf_a, mpf_b);
         mpf_mul(mpf_a, mpf_a, mpf_c);
         k += rand();
     }
     end_mpf = clock();
     time_taken_mpf = (((long double)(end_mpf - start_mpf)) / CLOCKS_PER_SEC) - loop_seconds;
     //printf("k is %d |", k);
     gmp_printf("mpf answer: %.Ff |",mpf_a);
     printf("mpf time: %Lf |",time_taken_mpf);

    //unum calculation
     k=0;
     x2u(10.0001, &ua.left_bound);
     x2u(1.0002, &ub.left_bound);
     x2u(0.9998, &uc.left_bound);
     start_u = clock();
     for (j = 0; j < repeat; j++) {
         timesu(&ua, ua.left_bound, ub.left_bound);
         timesu(&ua, ua.left_bound, uc.left_bound);
         k += rand();
     }
     end_u = clock();
     time_taken_unum = (((long double)(end_u - start_u)) / CLOCKS_PER_SEC) - loop_seconds;
     //printf("k is %d |", k);
     printf("unum answer: %e |",u2f(ua.left_bound));
     printf("unum time: %Lf |",time_taken_unum);

     printf("\n");
 //}*/
}
